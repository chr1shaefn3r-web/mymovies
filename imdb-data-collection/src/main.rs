extern crate reqwest;
extern crate select;

use select::document::Document;
use select::node::Node;
use select::predicate::Name;
use std::env;
use std::io::{self, Write};

fn main() {
    let args: Vec<String> = env::args().collect();
    print_ids(args.get(1).unwrap().to_string());
}

fn print_ids(url: String) {
    let resp = get_page_body(url);
    let body = resp.unwrap();

    let document = Document::from(body.as_ref());
    let mut ids: Vec<String> = document
        .find(Name("a"))
        .filter_map(href)
        .filter(is_movie_link)
        .filter_map(get_movie_id)
        .map(wrap_in_quotation_marks)
        .collect();
    ids.dedup();

    print_as_json_array(ids);
}

fn get_page_body(url: String) -> Result<std::string::String, Box<dyn std::error::Error>> {
    Ok(reqwest::blocking::get(url)?.text()?)
}

fn print_as_json_array(ids: Vec<String>) {
    print!("[");
    print!("{}", ids.join(", "));
    print!("]");
    io::stdout().flush().unwrap();
}

fn href(n: Node) -> Option<&str> {
    n.attr("href")
}

fn is_movie_link(s: &&str) -> bool {
    s.starts_with("/title/tt")
}

fn get_movie_id(s: &str) -> Option<&str> {
    s.get(7..16)
}

fn wrap_in_quotation_marks(s: &str) -> String {
    format!("\"{}\"", s)
}
