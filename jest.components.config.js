const basicConfig = require("./jest.config");

module.exports = Object.assign({}, basicConfig, {
    setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
    moduleNameMapper: {
        "\\.(css|jpg|png|svg)$": "<rootDir>/node_modules/jest-css-modules"
    },
    testEnvironment: "jest-environment-jsdom"
})
