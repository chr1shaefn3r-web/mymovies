import Header from "../../components/header/header";
import CollectionsSubNavigation from "../../components/nav/collectionsSubNavigation"
import CollectionWithProgress from "../../components/collectionWithProgress"

import oscarBestPicture from "../../api/v1/collections/oscarBestPicture/oscarBestPictures.js"

export async function getStaticProps() {
  return {
    props: oscarBestPicture()
  };
}

function Index(props) {
  const title = "Oscar Best Picture";
  return (
    <main>
      <Header title={title}
        externalTitleLink="https://www.imdb.com/search/title/?count=100&groups=oscar_best_picture_winners&sort=year,desc&ref_=nv_ch_osc">
        <CollectionsSubNavigation title={title} />
      </Header>
      <CollectionWithProgress {...props} />
    </main>
  );
}

export default Index;
