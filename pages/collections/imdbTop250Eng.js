import Header from "../../components/header/header";
import CollectionsSubNavigation from "../../components/nav/collectionsSubNavigation"
import CollectionWithProgress from "../../components/collectionWithProgress"

import imdbTop250Eng from "../../api/v1/collections/imdbTop250Eng/imdbTop250Eng.js"

export async function getStaticProps() {
  return {
    props: imdbTop250Eng()
  };
}

function Index(props) {
  const title = "IMDB Top250 Eng";
  return (
    <main>
      <Header title={title}
        externalTitleLink="https://www.imdb.com/chart/top-english-movies">
        <CollectionsSubNavigation title={title} />
      </Header>
      <CollectionWithProgress {...props} />
    </main>
  );
}

export default Index;
