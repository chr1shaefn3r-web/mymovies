import Header from "../../components/header/header";
import CollectionsSubNavigation from "../../components/nav/collectionsSubNavigation"
import CollectionWithProgress from "../../components/collectionWithProgress"

import imdbTop250 from "../../api/v1/collections/imdbTop250/imdbTop250.js"

export async function getStaticProps() {
  return {
    props: imdbTop250()
  };
}

function Index(props) {
  const title = "IMDB Top250";
  return (
    <main>
      <Header title={title}
        externalTitleLink="https://www.imdb.com/chart/top?ref_=nv_mv_250">
        <CollectionsSubNavigation title={title} />
      </Header>
      <CollectionWithProgress {...props} />
    </main>
  );
}

export default Index;
