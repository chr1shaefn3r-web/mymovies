import Header from "../../components/header/header";
import CollectionsSubNavigation from "../../components/nav/collectionsSubNavigation"
import Collections from "../../components/collections"

import sortMissingCollections from "../../lib/sortMissingCollections"

import MissingCollections from "../../api/v1/collections/missing/missing"

export async function getStaticProps() {
  // For whatever reason next.js thinks "MissingCollections" is not serializable:
  // SerializableError: Error serializing `.collections[74].foundMovies[2]` returned from `getStaticProps` in "/collections".
  // Reason: Circular references cannot be expressed in JSON.
  // JSON.stringify on the other hand has no problem with it
  const serializableMissingCollections = JSON.parse(JSON.stringify(MissingCollections()));
  return {
    props: {
      collections: serializableMissingCollections
    }
  };
}

function Index({ collections }) {
  const title = "Collections";
  return (
    <main>
      <Header title={title} >
        <CollectionsSubNavigation title={title} />
      </Header>
      <Collections sortedCollections={sortMissingCollections(collections)} />
    </main>
  );
}

export default Index;
