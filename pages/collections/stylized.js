import Header from "../../components/header/header";
import CollectionsSubNavigation from "../../components/nav/collectionsSubNavigation"
import StylizedCollections from "../../collections/stylized/stylizedCollections"

import stylizedCollections from "../../api/v1/collections/stylized/stylized"

export async function getStaticProps() {
    return {
        props: {
            collections: stylizedCollections()
        }
    };
}

function Stylized({ collections }) {
    const title = "Stylized";
    return (
        <main>
            <Header title={title} >
                <CollectionsSubNavigation title={title} />
            </Header>
            <StylizedCollections sortedCollections={collections} />
        </main>
    );
}

export default Stylized;
