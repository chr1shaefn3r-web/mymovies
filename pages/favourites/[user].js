import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Header from '../../components/header/favouritesHeader'
import FavouriteMovies from '../../components/favouriteMovies'

export async function getStaticProps({ params: { user } }) {
    const FavouriteMovies = require(`../../api/v1/favourites/${user}/favourites`)
    return {
        props: FavouriteMovies()
    }
}

export async function getStaticPaths() {
    return {
        paths: [
            { params: { user: "chr1shaefn3r" } }
        ],
        fallback: false
    };
}

function User(props) {
    const [state, setState] = useState({ localMoviesDifferFromServer: false, ...props });
    const { query: { user } } = useRouter();

    useEffect(() => {
        const localMovies = JSON.parse(window.localStorage.getItem(user));
        if (localMovies) {
            const localMoviesDifferFromServer = localMovies.length !== state.movies.length;
            setState({ movies: localMovies, localMoviesDifferFromServer });
        }
    }, [props.movies, user, state.movies.length]);

    return (
        <main>
            <Header title={user} />
            <FavouriteMovies user={user} {...state} />
        </main>
    );
}

export default User;
