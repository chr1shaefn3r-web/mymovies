import Link from "next/link"
import Header from "../components/header/header"
import Collection from "../components/collection"

import home from "../api/v1/bff/home/home"

export async function getStaticProps() {
  return {
    props: home()
  };
}

function Index({ lastAddedMovies, closeToCompletedCollections, missingImdbTop250EngOscarBestPictureWinners }) {
  return (
    <main>
      <Header title="Home" />
      <section>
        <h2>Last Added Movies</h2>
        <Collection foundMovies={lastAddedMovies} />
      </section>
      {!!closeToCompletedCollections.length &&
        <section>
          <h2>Close to Completed Collections</h2>
          {
            closeToCompletedCollections.map((collection) => <Collection {...collection} key={collection.name} />)
          }
        </section>
      }
      {!!missingImdbTop250EngOscarBestPictureWinners.length &&
        <section>
          <h2>Missing Imdb Top 250 Eng Oscar Best Picture Winners</h2>
          <ol>
            {
              missingImdbTop250EngOscarBestPictureWinners.map((movieId) => <li key={movieId}><a href={`https://www.imdb.com/title/${movieId}`}><div className={`touchsized`}>{movieId}</div></a></li>)
            }
          </ol>
        </section>
      }
      <section>
        <h2>Links</h2>
        <a href="https://bestsimilar.com/"><div className={`touchsized linkTouchsized`}>bestsimilar.com,&nbsp;</div></a>
        <a href="https://www.themoviedb.org/"><div className={`touchsized linkTouchsized`}>www.themoviedb.org,&nbsp;</div></a>
        <a href="https://www.ratingraph.com/"><div className={`touchsized linkTouchsized`}>www.ratingraph.com,&nbsp;</div></a>
        <Link legacyBehavior href="/movies/byMediaType" passHref><a><div className={`touchsized linkTouchsized`}>byMediaType,&nbsp;</div></a></Link>
        <Link legacyBehavior href="/collections/stylized" passHref><a><div className={`touchsized linkTouchsized`}>Stylized collections</div></a></Link>
      </section>
      <style jsx>{`
        .linkTouchsized {
          display: inline-block;
          vertical-align: middle;
        }
        /* aligned with bootstrap grid category large */
        @media screen and (max-width: 768px) { /* chrome device tablet */
          .touchsized {
            min-height: 48px;
            min-width: 48px;
            font-size: larger;
          }
        }
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            section {
                max-width: 80%;
                margin-left: auto;
                margin-right: auto;
            }
        }
      `}</style>
    </main>
  );
}

export default Index;
