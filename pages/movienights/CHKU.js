import Header from "../../components/header/header";
import MoviesSubNavigation from "../../components/nav/moviesSubNavigation"
import Collection from "../../components/collection"

import CHKU from "../../api/v1/movieNights/CHKU/CHKU"

export async function getStaticProps() {
  return {
    props: CHKU()
  };
}

function Index(props) {
  const title = "Nights";
  const name = "CHKU";
  return (
    <main>
      <Header title={title} >
        <MoviesSubNavigation title={title} />
      </Header>
      <div className={name}>
          <Collection key={name} id={name} name={name} foundMovies={props.movies} />

          <style jsx>{`
              /* aligned with bootstrap grid category large */
              @media screen and (min-width: 992px) {
                  .${name} {
                      max-width: 80%;
                      margin: auto;
                  }
              }
          `}</style>
      </div >
    </main>
  );
}

export default Index;
