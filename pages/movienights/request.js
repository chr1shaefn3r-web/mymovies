import Header from "../../components/header/header"
import MoviesSubNavigation from "../../components/nav/moviesSubNavigation"
import RequestMoviesForMovieNight from "../../components/requestMoviesForMovieNight"

import sortMovies from "../../lib/sortMovies"

import moviesByProperties from "../../api/v1/bff/movies/moviesByProperties"

export async function getServerSideProps() {
  return {
    props: {
      movies: moviesByProperties(["id", "file", "title", "poster", "year"])
    }
  };
}

// This is used so that the page can act as Single Page Application
// which needs shallow client-side navigation (no rerun of `getServerSideProps`)
let clientSideMovieCache = {};

function RequestMoviesForMovieNightPage({ movies }) {
  if (movies) {
    clientSideMovieCache = movies;
  }
  const subNavigation = "Nights";
  const pageTitle = "Request Movies for Movienight";
  return (
    <main>
      <Header title={pageTitle}>
        <MoviesSubNavigation title={subNavigation} />
      </Header>
      <RequestMoviesForMovieNight movies={sortMovies(movies || clientSideMovieCache)} />
    </main>
  );
}

export default RequestMoviesForMovieNightPage;
