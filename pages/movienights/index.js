import Header from "../../components/header/header"
import MoviesSubNavigation from "../../components/nav/moviesSubNavigation"
import MovieNights from "../../components/movienights"

function Index() {
  const title = "Nights";
  return (
    <main>
      <Header title={title}>
        <MoviesSubNavigation title={title} />
      </Header>
      <MovieNights />
    </main>
  );
}

export default Index;
