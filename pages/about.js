import { Component } from "react";
import Header from "../components/header/header";
import About from "../components/about";

class AboutPage extends Component {
  render() {
    return (
      <main>
        <Header title="About" />
        <About />
      </main>
    );
  }
}

export default AboutPage;