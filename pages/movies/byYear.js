import Header from "../../components/header/header"
import MoviesSubNavigation from "../../components/nav/moviesSubNavigation"
import MoviesByYear from "../../components/moviesbyYear"

import byYear from "../../api/v1/movies/byYear/byYear"

export async function getStaticProps() {
  return {
    props: {
      moviesByYear: byYear()
    }
  };
}

function Index({ moviesByYear }) {
  const title = "By Year";
  return (
    <main>
      <Header title={title}>
        <MoviesSubNavigation title={title} />
      </Header>
      <MoviesByYear moviesByYear={moviesByYear} />
    </main>
  );
}

export default Index;
