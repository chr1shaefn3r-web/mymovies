import Header from "../../components/header/header"
import MoviesSubNavigation from "../../components/nav/moviesSubNavigation"
import MoviesByActor from "../../components/moviesbyActor"

import byActor from "../../api/v1/movies/byActor/byActor"

export async function getStaticProps() {
  // For whatever reason next.js things "byGenre()" is not serializable:
  // SerializableError: Error serializing `.moviesByActor[8].movies[1]` returned from `getStaticProps` in "/movies/byActor".
  // Reason: Circular references cannot be expressed in JSON.
  // JSON.stringify on the other hand has no problem with it
  const serializableMoviesbyActor = JSON.parse(JSON.stringify(byActor()));
  return {
    props: {
      moviesByActor: serializableMoviesbyActor
    }
  };
}

function Index({ moviesByActor }) {
  const title = "By Actor";
  return (
    <main>
      <Header title={title}>
        <MoviesSubNavigation title={title} />
      </Header>
      <MoviesByActor moviesByActor={moviesByActor} />
    </main>
  );
}

export default Index;
