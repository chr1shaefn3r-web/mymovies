import Header from "../../components/header/header"
import MoviesSubNavigation from "../../components/nav/moviesSubNavigation"
import Movies from "../../components/movies"

import movies from "../../api/v1/bff/movies/movies"

export async function getStaticProps() {
  return {
    props: {
      movies: movies()
    }
  };
}

function Index({ movies }) {
  const title = "Movies";
  return (
    <main>
      <Header title={title} >
        <MoviesSubNavigation title={title} />
      </Header>
      <Movies sortedMovies={movies} />
    </main>
  );
}

export default Index;
