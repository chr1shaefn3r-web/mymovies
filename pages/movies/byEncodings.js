import Header from "../../components/header/header"
import MoviesSubNavigation from "../../components/nav/moviesSubNavigation"
import MoviesByEncodings from "../../components/moviesByEncodings"

import byEncodings from "../../api/v1/movies/byEncodings/byEncodings"

export async function getStaticProps() {
  return {
    props: {
      moviesByEncodings: byEncodings()
    }
  };
}

function Index({ moviesByEncodings }) {
  const title = "By Encodings";
  return (
    <main>
      <Header title={title}>
        <MoviesSubNavigation title={title} />
      </Header>
      <MoviesByEncodings moviesByEncodings={moviesByEncodings} />
    </main>
  );
}

export default Index;
