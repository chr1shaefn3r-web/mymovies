import Header from "../../../components/header/header"
import MoviesSubNavigation from "../../../components/nav/moviesSubNavigation"
import MoviesByMediaType from "../../../components/moviesbyMediaType"

import byMediaType from "../../../api/v1/movies/byMediaType/byMediaType"

export async function getStaticProps() {
  return {
    props: {
      moviesByMediaType: byMediaType()
    }
  };
}

function Index({ moviesByMediaType }) {
  const title = "By MediaType";
  return (
    <main>
      <Header title={title}>
        <MoviesSubNavigation title={title} />
      </Header>
      <MoviesByMediaType moviesByMediaType={moviesByMediaType} />
    </main>
  );
}

export default Index;
