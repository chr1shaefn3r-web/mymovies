import Header from "../../components/header/header"
import MoviesSubNavigation from "../../components/nav/moviesSubNavigation"
import MoviesByDirector from "../../components/moviesbyDirector"

import byDirector from "../../api/v1/movies/byDirector/byDirector"

export async function getStaticProps() {
  // For whatever reason next.js things "byGenre()" is not serializable:
  // SerializableError: Error serializing `.moviesByActor[8].movies[1]` returned from `getStaticProps` in "/movies/byActor".
  // Reason: Circular references cannot be expressed in JSON.
  // JSON.stringify on the other hand has no problem with it
  const serializableMoviesbyDirector = JSON.parse(JSON.stringify(byDirector()));
  return {
    props: {
      moviesByDirector: serializableMoviesbyDirector
    }
  };
}

function Index({ moviesByDirector }) {
  const title = "By Director";
  return (
    <main>
      <Header title={title}>
        <MoviesSubNavigation title={title} />
      </Header>
      <MoviesByDirector moviesByDirector={moviesByDirector} />
    </main>
  );
}

export default Index;
