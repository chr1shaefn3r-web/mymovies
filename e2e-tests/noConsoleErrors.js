const process = require("process");
const puppeteer = require('puppeteer');

const baseUrl = "https://mymovies-chr1shaefn3r.vercel.app/";

async function testForConsoleErrorsOn(url) {
    const browser = await puppeteer.launch();

    const page = await browser.newPage();

    let receivedConsoleError = false;
    page
        .on('console', message => {
            const messageType = message.type().substr(0, 3).toUpperCase();
            receivedConsoleError = messageType === "ERR"
            if (receivedConsoleError) {
                console.log(`${messageType} ${message.text()}`);
            }
        });

    await page.goto(url);
    await browser.close();
    console.log("Result: ", receivedConsoleError);
    return receivedConsoleError;
}

async function noConsoleErrorsOn(urls) {
    for (let url of urls) {
        console.log("Test url: ", url);
        if (await testForConsoleErrorsOn(baseUrl + url)) return true;
    }
    return false;
}

noConsoleErrorsOn([
    "/",
    "/movies",
    "/movies/byYear",
    "/collections",
    "/collections/imdbTop250",
    "/about"
]).then((foundErrors) => {
    if (foundErrors) {
        process.exit(1);
    }
});
