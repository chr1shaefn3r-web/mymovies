import Collection from "./collection";

const StylizedCollections = ({ sortedCollections }) => <div className="stylizedCollections">
    {
        sortedCollections.map((collection) => <Collection key={collection.name} {...collection} />)
    }

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .stylizedCollections {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default StylizedCollections;
