import MoviePoster from "./moviePoster"

const CollectionEntry = ({ title, url }) => <div className="small-img">
    <MoviePoster title={title} url={url}/>

    <style jsx>{`
        .small-img {
            position: relative;
            display: inline-block;
            vertical-align: middle;
            background-color: transparent;
            margin: .25rem;
        }
    `}</style>
</div >;

export default CollectionEntry;
