import CollectionEntry from "./collectionEntry"
import Title from "../../components/title"

const Collection = ({ name, movies = [] }) => <div className="collection">
    {name && <Title title={name} />}
    {
        movies.map(({ id, title, poster }) =>
            <CollectionEntry key={id} id={id} title={title} url={poster} />
        )
    }

    <style jsx>{`
        .collection {
            padding-bottom: 1rem;
            border-bottom: 1px solid #dee2e6;
        }
    `}</style>
</div >;

export default Collection;
