const https = (url) => url.replace("http://", "https://");

const MoviePoster = ({ url, title }) => <a href={https(url)} target='_blank' rel="noreferrer">
    <img loading="lazy" width="200px" height="300px" src={https(url).replace("original", "w200")} alt={title + " Poster"} />
</a>;

export default MoviePoster;
