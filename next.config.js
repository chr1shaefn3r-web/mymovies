const withOffline = require('next-offline');

const oneDay = 24 * 60 * 60;
const oneMonth = 30 * oneDay;
const oneWeek = 7 * oneDay;

const nextConfig = {
  reactStrictMode: true,
  images: {
    imageSizes: [150],
    domains: ['image.tmdb.org'],
  },
  transformManifest: manifest => ['/'].concat(manifest), // add the homepage to the cache
  workboxOpts: {
    swDest: 'static/service-worker.js',
    runtimeCaching: [
      {
        urlPattern: new RegExp('https://image\\.tmdb\\.org/t/p/w200/.*\\.jpg'),
        handler: 'CacheFirst',
        options: {
          cacheName: 'posters',
          expiration: {
            maxEntries: 100,
            maxAgeSeconds: oneMonth,
            purgeOnQuotaError: true
          },
          cacheableResponse: {
            statuses: [0, 200],
          }
        },
      },
      {
        urlPattern: /_next\/data.*\.json$/,
        handler: 'CacheFirst',
        options: {
          cacheName: 'json-calls',
          expiration: {
            maxEntries: 50,
            maxAgeSeconds: oneMonth,
            purgeOnQuotaError: true
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        },
      },
      {
        urlPattern: /^https?.*/,
        handler: 'StaleWhileRevalidate',
        options: {
          cacheName: 'https-calls',
          expiration: {
            maxEntries: 50,
            maxAgeSeconds: oneWeek,
            purgeOnQuotaError: false
          },
          cacheableResponse: {
            statuses: [0, 200],
          },
        },
      }
    ],
  },
};

module.exports = withOffline(nextConfig);
