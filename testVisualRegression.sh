#!/bin/bash

set -e # Exit immediately if a simple command exits with a non-zero status
set -x # Print the command before executing it

vercel --target="staging"
backstop --config="backstop.config.js" test

