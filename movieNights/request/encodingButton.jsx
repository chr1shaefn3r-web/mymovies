export default ({ name, amountOfMoviesAvailableForThisEncoding, selected, color, onEncodingSelected }) => {
    return <>
        <button key={name} className={`md-item md-button ${selected ? "md-unelevated" : "md-text"}`}
            onClick={() => onEncodingSelected(!selected)}
        >
            {name} ({amountOfMoviesAvailableForThisEncoding} Movie{amountOfMoviesAvailableForThisEncoding > 1 ? "s" : ""})
        </button>
        <style jsx>{`
            .md-unelevated {
                background-color: ${color};
            }
            .md-text {
                color: ${color};
                background-color: transparent;
            }
            .md-item {
                justify-content: center;
                user-select: none;
                font-family: Roboto,sans-serif;
                overflow: hidden;
                display: inline-flex;
                align-items: center;
            }
            .md-button {
                height: 36px;
                letter-spacing: .0892857143em;
                font-size: .875rem;
                font-weight: 500;
                padding: 0 16px;
                margin: 16px 0px;
                margin-right: 8px;
                border: none;
                border-radius: 4px;
            }
        `}</style>
    </>
}