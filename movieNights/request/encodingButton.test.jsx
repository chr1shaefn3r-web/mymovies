import React from 'react'
import { render, fireEvent } from '@testing-library/react'

import EncodingButton from './encodingButton'

const oneMovie = 1;
const multipleMovies = 2;
const selected = true;
const unselected = false;
const encodingName = "h264"

test('display selected button with name and call callback on click with new selected state', async () => {
    const { click, mockCallback } = renderEncodingButton(encodingName, selected, oneMovie)
    click(encodingName)
    expect(mockCallback).toBeCalledWith(unselected)
})

test('display unselected button with name and call callback on click with new selected state', async () => {
    const { click, mockCallback } = renderEncodingButton(encodingName, unselected, oneMovie)
    click(encodingName)
    expect(mockCallback).toBeCalledWith(selected)
})

test('display button for multiple movies and show "Movies" in the UI', async () => {
    const { getByRegex } = renderEncodingButton(encodingName, selected, multipleMovies)
    expect(getByRegex("Movies")).toBeInTheDocument()
})

test('display selected button with given color as background-color', async () => {
    const color = "red";
    const { container } = renderEncodingButton(encodingName, selected, multipleMovies, color)
    expect(container.firstChild).toHaveStyle(`background-color: ${color};`);
})

test('display unselected button with given color as text color', async () => {
    const color = "green";
    const { container } = renderEncodingButton(encodingName, unselected, multipleMovies, color)
    expect(container.firstChild).toHaveStyle(`color: ${color};`);
})

function renderEncodingButton(name, selected, amountOfMoviesAvailableForThisEncoding, color) {
    const mockCallback = jest.fn();
    const { getByText, container } = render(
        <EncodingButton name={name} selected={selected} color={color}
            amountOfMoviesAvailableForThisEncoding={amountOfMoviesAvailableForThisEncoding} onEncodingSelected={mockCallback} />)
    const getByRegex = (text) => {
        return getByText(new RegExp(text, "i"))
    }
    const click = (text) => {
        fireEvent.click(getByRegex(text))
    }
    return { click, mockCallback, getByRegex, container }
}