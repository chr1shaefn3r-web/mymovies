import noDot from "./noDot"

describe("noDot", () => {

    it('should be defined', () => {
        expect(noDot).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof noDot).toBe("function");
    });

    it('should remove first dot from given string', () => {
        const textWithDot = "uhd.h265";
        const textWithoutDot = "uhdh265";
        expect(noDot(textWithDot)).toBe(textWithoutDot);
    });

    it('should remove all dots from given string', () => {
        const textWithDot = "filme.uhd.h265";
        const textWithoutDot = "filmeuhdh265";
        expect(noDot(textWithDot)).toBe(textWithoutDot);
    });
});
