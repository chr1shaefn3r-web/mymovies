import React from 'react'
import { render } from '@testing-library/react'

import ProgressbarItem from './progressbarItem'

const h264Encoding = "h264";
const mockPercentage = 13.37;
const color = "green";

test('display item with given color as background-color', async () => {
    const { renderedComponent } = renderProgressbarItem(h264Encoding, mockPercentage, color)
    expect(renderedComponent).toHaveStyle(`background-color: ${color};`);
})

test('display item with given percentage as width in percent', async () => {
    const { renderedComponent } = renderProgressbarItem(h264Encoding, mockPercentage, color)
    expect(renderedComponent).toHaveStyle(`width: ${mockPercentage}%;`);
})

function renderProgressbarItem(name, percentage, color) {
    const { container } = render(
        <ProgressbarItem name={name} percentage={percentage} color={color} />)
    return { renderedComponent: container.firstChild }
}
