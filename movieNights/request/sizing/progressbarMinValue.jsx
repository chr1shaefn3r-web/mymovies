import styles from "./progressbar.module.css"

export default ({ value }) =>
    <>
        <div className={`${styles.progressbarContainerBorderColorLeft} progressbar-min-value`}>
            {value}
        </div>
        <style jsx>{`
            .progressbar-min-value {
                padding-top: .25em;
                padding-left: .5em;
                float: left;
            }
        `}</style>
    </>