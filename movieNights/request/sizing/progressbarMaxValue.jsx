import styles from "./progressbar.module.css"

export default ({ value }) =>
    <>
        <div className={`${styles.progressbarContainerBorderColorRight} progressbar-max-value`}>
            {value}
        </div>
        <style jsx>{`
            .progressbar-max-value {
                padding-top: .25em;
                padding-right: .5em;
                float: right;
            }
        `}</style>
    </>