import styles from "./progressbar.module.css"
import noDot from "./noDot"

export default ({ name, color, percentage }) => {
    const cssSuffix = noDot(name);
    return <>
        <div className={`${styles.progressbarItem} progressbar-width-${cssSuffix} progressbar-color-${cssSuffix}`}>&nbsp;</div>
        <style jsx>{`
            .progressbar-width-${cssSuffix} {
                width: ${percentage}%;
            }
            .progressbar-color-${cssSuffix} {
                background-color: ${color};
            }
        `}</style>
    </>
}