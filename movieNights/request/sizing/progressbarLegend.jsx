import styles from "./progressbar.module.css"
import humanReadableFilesize from "../../../lib/humanReadableFileSize"
import noDot from "./noDot"

export default ({ name, color, amountOfMoviesAvailableForThisEncoding, size }) => {
    const cssSuffix = noDot(name);
    return <>
        <div className={`${styles.progressbarItem} progressbar-legend progressbar-color-${cssSuffix}`}>
            {name}: {amountOfMoviesAvailableForThisEncoding} Movie{amountOfMoviesAvailableForThisEncoding > 1 ? "s" : ""} ({humanReadableFilesize(size)})
        </div>
        <style jsx>{`
            .progressbar-legend {
                margin-top: .75em;
                margin-bottom: .75em;
                margin-right: .75em;
                font-family: Sans-Serif;
            }

            .progressbar-color-${cssSuffix} {
                background-color: ${color};
            }
        `}</style>
    </>
}