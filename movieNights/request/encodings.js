const h265FileSizeMap = require("../../api/v1/movies/movieFilesizeMap.h265.json");
const h265StereoFileSizeMap = require("../../api/v1/movies/movieFilesizeMap.h265.stereo.json");
const brRawFileSizeMap = require("../../api/v1/movies/movieFilesizeMap.br.raw.json");
const uhdH265FileSizeMap = require("../../api/v1/movies/movieFilesizeMap.uhd.h265.json");
const uhdRawFileSizeMap = require("../../api/v1/movies/movieFilesizeMap.uhd.raw.json");

export default [{
    name: "h264",
    fileSizeMap: require("../../api/v1/movies/movieFilesizeMap.json"),
    searchDirectory: "/home/share/filme/",
    filter: () => true,
    encodingSpecificFilename: (file) => file,
    color: "#5c6bc0", // indigo-400
    defaultSelected: true,
}, {
    name: "h265",
    fileSizeMap: h265FileSizeMap,
    searchDirectory: "/home/share/filme.h265/",
    filter: (movie) => !!h265FileSizeMap[movie.id],
    encodingSpecificFilename: (file) => file.replace('mkv', 'h265.mkv'),
    color: "#ff7043", // deep-orange-400
    defaultSelected: true,
}, {
    name: "h265.stereo",
    fileSizeMap: h265StereoFileSizeMap,
    searchDirectory: "/home/share/filme.h265.stereo/",
    filter: (movie) => !!h265StereoFileSizeMap[movie.id],
    encodingSpecificFilename: (file) => file.replace('mkv', 'h265.stereo.mkv'),
    color: "#66bb6a", // green-400
    defaultSelected: false,
}, {
    name: "br.raw",
    fileSizeMap: brRawFileSizeMap,
    searchDirectory: "/raw/filme.br.raw/",
    filter: (movie) => !!brRawFileSizeMap[movie.id],
    encodingSpecificFilename: (file) => file.replace('mkv', 'br.raw.mkv'),
    color: "#6d4c41", // brown-600
    defaultSelected: false,
}, {
    name: "uhd.h265",
    fileSizeMap: uhdH265FileSizeMap,
    searchDirectory: "/home/share/filme.uhd.h265/",
    filter: (movie) => !!uhdH265FileSizeMap[movie.id],
    encodingSpecificFilename: (file) => file.replace('mkv', 'uhd.h265.mkv'),
    color: "#26a69a", // teal-400
    defaultSelected: true,
}, {
    name: "uhd.raw",
    fileSizeMap: uhdRawFileSizeMap,
    searchDirectory: "/raw/filme.uhd.raw/",
    filter: (movie) => !!uhdRawFileSizeMap[movie.id],
    encodingSpecificFilename: (file) => file.replace('mkv', 'uhd.raw.mkv'),
    color: "#7e57c2", // deep-purple-400
    defaultSelected: false,
}]
