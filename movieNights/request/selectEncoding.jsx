import EncodingButton from "./encodingButton";

const SelectEncoding = ({ encodingsWithSizes, selectedEncodings, onEncodingSelected }) => {
    return <div>
        {encodingsWithSizes.map(({ name, color, amountOfMoviesAvailableForThisEncoding }) =>
            <EncodingButton key={name} name={name} color={color}
                amountOfMoviesAvailableForThisEncoding={amountOfMoviesAvailableForThisEncoding}
                selected={selectedEncodings.includes(name)}
                onEncodingSelected={(newButtonSelectedState) => {
                    let newSelectedEncodings;
                    if (newButtonSelectedState) {
                        newSelectedEncodings = selectedEncodings.concat(name)
                    } else {
                        newSelectedEncodings = selectedEncodings.filter((encoding) => encoding != name)
                    }
                    onEncodingSelected(newSelectedEncodings);
                }}
            />
        )}
    </div>
};

export default SelectEncoding;