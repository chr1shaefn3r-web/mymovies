import React from 'react'
import { render, fireEvent } from '@testing-library/react'

import SelectEncoding from './selectEncoding'

const h264Encoding = "h264";
const h265Encoding = "h265";
const h264EncodingWithSizes = [{ name: h264Encoding }];
const h264AndH265EncodingWithSizes = [{ name: h264Encoding }, { name: h265Encoding }];

test('display one selected encoding button and call callback on click without encodings', async () => {
    const selectedEncodings = [h264Encoding]
    const { click, mockCallback } = renderSelectEncoding(h264EncodingWithSizes, selectedEncodings)

    click(h264Encoding)

    expect(mockCallback).toBeCalledWith([])
})

test('display two selected encoding buttons and call callback on click with selected encoding', async () => {
    const selectedEncodings = [h264Encoding, h265Encoding];
    const { click, mockCallback } = renderSelectEncoding(h264AndH265EncodingWithSizes, selectedEncodings)

    click(h264Encoding)

    expect(mockCallback).toBeCalledWith([h265Encoding])
})

test('display two encoding buttons one selected and call callback on click with selected encoding', async () => {
    const selectedEncodings = [h264Encoding];
    const { click, mockCallback } = renderSelectEncoding(h264AndH265EncodingWithSizes, selectedEncodings)

    click(h265Encoding)

    expect(mockCallback).toBeCalledWith([h264Encoding, h265Encoding])
})

function renderSelectEncoding(encodingsWithSizes, selectedEncodings) {
    const mockCallback = jest.fn();
    const { getByText } = render(
        <SelectEncoding encodingsWithSizes={encodingsWithSizes} selectedEncodings={selectedEncodings} onEncodingSelected={mockCallback} />)
    const click = (text) => {
        fireEvent.click(getByText(new RegExp(text, "i")))
    }
    return { click, mockCallback }
}