const titleSpecialTreatment = (title) => {
    let noPrefix = title.replace(/^(Der|Die|Das|The)/, '');
    let noAUmlaut = noPrefix.replace(/Ä/, 'A');
    let noUUmlaut = noAUmlaut.replace(/Ü/, 'U');
    let noOUmlaut = noUUmlaut.replace(/Ö/, 'O');
    let noBrackets = noOUmlaut.replace(/\(|\)/, '');
    return noBrackets.trim().toLowerCase();
};

export default titleSpecialTreatment;
