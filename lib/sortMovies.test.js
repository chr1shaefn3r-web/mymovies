import sortMovies from "./sortMovies";

describe("sortMovies", () => {

	it('should return an array', () => {
		expect(Array.isArray(sortMovies())).toBe(true);
		expect(Array.isArray(sortMovies({}))).toBe(true);
		expect(Array.isArray(sortMovies([]))).toBe(true);
	});

	const getSortedMockMovieList = () => [
		{ title: '96 Hours - Taken 2 (2012)' },
		{ title: 'American Hustle' },
		{ title: 'Men in Black (1997)' },
		{ title: 'Moneyball - Die Kunst zu gewinnen' }
	];
	it('should not change a already sorted list', () => {
		const expected = getSortedMockMovieList();
		const actual = sortMovies(getSortedMockMovieList());
		expect(actual).toEqual(expected);
	});

	it('should sort a list', () => {
		const mockUnsortedMovieList = [
			{ title: 'Men in Black (1997)' },
			{ title: '96 Hours - Taken 2 (2012)' },
			{ title: 'Moneyball - Die Kunst zu gewinnen' },
			{ title: 'American Hustle' }
		];
		const expected = getSortedMockMovieList();
		expect(sortMovies(mockUnsortedMovieList)).toEqual(expected);
	});

	const getSortedMockMovieListWithDuplicate = () => [
		{ title: 'American Hustle' },
		{ title: 'American Hustle' }
	]
	it('should sort a list', () => {
		const expected = getSortedMockMovieListWithDuplicate();
		const actual = sortMovies(getSortedMockMovieListWithDuplicate());
		expect(actual).toEqual(expected);
	});

});
