'use strict';

const BYTE_UNITS = [
    'B',
    'kB',
    'MB',
    'GB'
];

module.exports = (number) => {
    if (!Number.isFinite(number)) {
        throw new TypeError(`Expected a finite number, got ${typeof number}: ${number}`);
    }

    const UNITS = BYTE_UNITS;

    if (number === 0) {
        return ' 0 ' + UNITS[0];
    }

    if (number < 1) {
        return number + ' ' + UNITS[0];
    }

    const exponent = Math.min(Math.floor(Math.log10(number) / 3), UNITS.length - 1);
    number = Number((number / Math.pow(1000, exponent)).toPrecision(3));

    const unit = UNITS[exponent];

    return number + ' ' + unit;
};
