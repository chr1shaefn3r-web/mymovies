import movieTitleSpecialTreatment from "./titleSpecialTreatment"

const sortMovies = (movies) => {
	if(!movies || !Array.isArray(movies)) {
		return [];
	}
	return sort(movies);
}

export default sortMovies;

const sort = (movies) => {
	return movies.sort((a, b) => {
		const first = movieTitleSpecialTreatment(a.title);
		const second = movieTitleSpecialTreatment(b.title);
		return (first<second?-1:(first>second?1:0))
	});
};
