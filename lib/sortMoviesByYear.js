const sortMoviesByYear = (movies) => {
	if (!movies || !Array.isArray(movies)) {
		return [];
	}
	return sort(movies);
}

export default sortMoviesByYear;

const sort = (movies) => {
	return movies.sort((a, b) => {
		return a.year - b.year;
	});
};
