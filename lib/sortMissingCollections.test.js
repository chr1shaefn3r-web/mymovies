import sortMissingCollections from "./sortMissingCollections";

describe("sortMissingCollections", () => {

	it('should return an array', () => {
		expect(Array.isArray(sortMissingCollections())).toBe(true);
		expect(Array.isArray(sortMissingCollections({}))).toBe(true);
		expect(Array.isArray(sortMissingCollections([]))).toBe(true);
	});

	const getMissingCollectionsListWithAllFoundMovies = () => [
		{ name: 'John Wick', missingMovies: [] },
		{ name: 'Matrix', missingMovies: [] },
		{ name: 'Star Wars', missingMovies: [] }
	];
	it('should not change a already sorted list without missingMovies information', () => {
		const expected = getMissingCollectionsListWithAllFoundMovies();
		const actual = sortMissingCollections(getMissingCollectionsListWithAllFoundMovies());
		expect(actual).toEqual(expected);
	});

	const getMissingCollectionsListWithAllFoundMoviesUnsorted = () => [
		{ name: 'Matrix', missingMovies: [] },
		{ name: 'John Wick', missingMovies: [] },
		{ name: 'Star Wars', missingMovies: [] }
	];
	it('should sort by name if no missingMovies information available', () => {
		const expected = getMissingCollectionsListWithAllFoundMovies();
		const actual = sortMissingCollections(getMissingCollectionsListWithAllFoundMoviesUnsorted());
		expect(actual).toEqual(expected);
	});

	const getMissingCollectionsListWithMissingMovies = () => [
		{ name: 'Avengers', missingMovies: [1] },
		{ name: 'Star Wars', missingMovies: [1] },
		{ name: 'John Wick', missingMovies: [] },
		{ name: 'Matrix', missingMovies: [] }
	];
	it('should not change a already sorted list', () => {
		const expected = getMissingCollectionsListWithMissingMovies();
		const actual = sortMissingCollections(getMissingCollectionsListWithMissingMovies());
		expect(actual).toEqual(expected);
	});

	const getMissingCollectionsListWithMissingMoviesUnsorted = () => [
		{ name: 'John Wick', missingMovies: [] },
		{ name: 'Star Wars', missingMovies: [1] },
		{ name: 'Avengers', missingMovies: [1] },
		{ name: 'Matrix', missingMovies: [] }
	];
	it('should sort by name and missingMovies', () => {
		const expected = getMissingCollectionsListWithMissingMovies();
		const actual = sortMissingCollections(getMissingCollectionsListWithMissingMoviesUnsorted());
		expect(actual).toEqual(expected);
	});

	const getMissingCollectionsListMixedCaseMovieNamesUnsorted = () => [
		{ name: 'X-Men', missingMovies: [] },
		{ name: 'Zurück in die Zukunft', missingMovies: [] },
		{ name: 'xXx', missingMovies: [] }
	];
	const getMissingCollectionsListMixedCaseMovieNamesSorted = () => [
		{ name: 'X-Men', missingMovies: [] },
		{ name: 'xXx', missingMovies: [] },
		{ name: 'Zurück in die Zukunft', missingMovies: [] }
	];
	it('should sort by lowercase name', () => {
		const expected = getMissingCollectionsListMixedCaseMovieNamesSorted();
		const actual = sortMissingCollections(getMissingCollectionsListMixedCaseMovieNamesUnsorted());
		expect(actual).toEqual(expected);
	});

	const getMissingCollectionsListWithSpecialPrefixInMovieNamesUnsorted = () => [
		{ name: 'Deadpool', missingMovies: [] },
		{ name: 'Terminator', missingMovies: [] },
		{ name: 'The Equalizer', missingMovies: [] }
	];
	const getMissingCollectionsListWithSpecialPrefixInMovieNamesSorted = () => [
		{ name: 'Deadpool', missingMovies: [] },
		{ name: 'The Equalizer', missingMovies: [] },
		{ name: 'Terminator', missingMovies: [] }
	];
	it('should sort without special prefixes in movie names', () => {
		const expected = getMissingCollectionsListWithSpecialPrefixInMovieNamesSorted();
		const actual = sortMissingCollections(getMissingCollectionsListWithSpecialPrefixInMovieNamesUnsorted());
		expect(actual).toEqual(expected);
	});
});
