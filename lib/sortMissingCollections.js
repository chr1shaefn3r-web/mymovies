import collectionNameSpecialTreatment from "./titleSpecialTreatment"

const sortMissingCollections = (missingCollections) => {
	if (!missingCollections || !Array.isArray(missingCollections)) {
		return [];
	}
	return missingCollections.sort(by({
		name: 'missingMovies',
		primer: hasMissingMovies,
		reverse: true
	}, {
		name: "name",
		primer: collectionNameSpecialTreatment,
	}));
}

export default sortMissingCollections

function hasMissingMovies(missingMovies) {
	return missingMovies.length > 0 ? 1 : 0;
}

function by(...fields) {
	return function (A, B) {
		let result;

		for (let i = 0; i < fields.length; i++) {
			result = 0;
			const field = fields[i];

			const { name } = field;
			let { reverse } = field;

			let a = A[name];
			let b = B[name];

			a = field.primer(a);
			b = field.primer(b);

			reverse = (field.reverse) ? -1 : 1;

			if (a < b) result = reverse * -1;
			if (a > b) result = reverse * 1;
			if (result !== 0) break;
		}
		return result;
	}
};