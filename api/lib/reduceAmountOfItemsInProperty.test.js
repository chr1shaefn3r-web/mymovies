const reduceAmountOfItemsInProperty = require("./reduceAmountOfItemsInProperty")

describe("reduceAmountOfItemsInProperty", () => {

    it('should be defined', () => {
        expect(reduceAmountOfItemsInProperty).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof reduceAmountOfItemsInProperty).toBe("function");
    });

    it('should return a "function"', () => {
        expect(typeof reduceAmountOfItemsInProperty()).toBe("function");
    });

    it('should do noting if given no parameter', () => {
        const transform = reduceAmountOfItemsInProperty();
        const inputObject = { a: [11, 22, 33] };
        const result = transform(inputObject);
        expect(result).toEqual(inputObject);
    });

    it('should reduce given property', () => {
        const transform = reduceAmountOfItemsInProperty("a", 1);
        const inputObject = { a: [11, 22, 33] };
        const result = transform(inputObject);
        expect(result).toEqual({
            a: [11]
        });
    });

    it('should not touch properties not given as parameter', () => {
        const transform = reduceAmountOfItemsInProperty("a", 1);
        const inputObject = { a: [11, 22, 33], b: 1 };
        const result = transform(inputObject);
        expect(result).toEqual({
            a: [11],
            b: 1
        });
    });
});
