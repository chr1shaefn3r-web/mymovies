const lookIdUpInMovies = require("./lookIdUpInMovies")

describe("allowProperties", () => {

    it('should be defined', () => {
        expect(lookIdUpInMovies).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof lookIdUpInMovies).toBe("function");
    });

    it('should return a type of "function"', () => {
        expect(typeof lookIdUpInMovies()).toBe("function");
    });

    it('should return an object with the given id if it is not part of the movies', () => {
        const id = "tt1234";
        const result = lookIdUpInMovies()(id);
        expect(result).toEqual({id});
    });

    it('should return the object from the movies for the given id', () => {
        const id = "tt1234";
        const result = lookIdUpInMovies([{
            id, poster: "http://example.com/"
        }])(id);
        expect(result).toEqual({
            id, poster: "http://example.com/"
        });
    });
});
