module.exports = transform =>
    nextReducer =>
        (accumulator, current) => nextReducer(accumulator, transform(current));