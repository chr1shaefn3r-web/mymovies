const compose = require("./compose")

describe("compose", () => {

    it('should be defined', () => {
        expect(compose).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof compose).toBe("function");
    });

    it('should return a "function"', () => {
        expect(typeof compose()).toBe("function");
    });

    it('should call all functions once the returned function is called', () => {
        const spy1 = jest.fn();
        const spy2 = jest.fn();
        const composed = compose(spy1, spy2);
        composed();
        expect(spy1).toHaveBeenCalledTimes(1);
        expect(spy2).toHaveBeenCalledTimes(1);
    });

    it('should call function with the given argument', () => {
        const spy1 = jest.fn();
        const mockArg = "testValue";
        const composed = compose(spy1);
        composed(mockArg);
        expect(spy1).toHaveBeenCalledWith(mockArg);
    });
});
