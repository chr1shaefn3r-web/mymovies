const collectionsFromMovies = require("./collectionsFromMovies")

describe("collectionsFromMovies", () => {

    it('should be defined', () => {
        expect(collectionsFromMovies).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof collectionsFromMovies).toBe("function");
    });

    it('should return an empty array given nothing', () => {
        expect(Array.isArray(collectionsFromMovies())).toBe(true);
    });

    it('should ignore collections with only one movie', () => {
        const mockMovie = getMockMovie("tt1234", 2008, "mockCollection")
        expect(collectionsFromMovies([mockMovie])).toEqual([]);
    });

    it('should ignore a given movie without set', () => {
        const mockMovie = {
            id: "tt1234"
        }
        expect(collectionsFromMovies([mockMovie])).toEqual([]);
    });

    it('should ignore a given movie without set name', () => {
        const mockMovie = getMockMovie("tt1234")
        expect(collectionsFromMovies([mockMovie])).toEqual([]);
    });

    it('should put two movies with the same set name in the same collection', () => {
        const mockMovieOne = getMockMovie("tt1234", 2008, "mockCollection")
        const mockMovieTwo = getMockMovie("tt5678", 2009, "mockCollection")

        const expectedCollection = {
            name: mockMovieOne.set.name,
            movies: [mockMovieOne.id, mockMovieTwo.id]
        }
        expect(collectionsFromMovies([mockMovieOne, mockMovieTwo])).toEqual([expectedCollection]);
    });

    it('should put two movies with the same set name in the same collection ordered by year', () => {
        const mockMovieOne = getMockMovie("tt1234", 2042, "mockCollection")
        const mockMovieTwo = getMockMovie("tt5678", 2009, "mockCollection")

        const expectedCollection = {
            name: mockMovieOne.set.name,
            movies: [mockMovieTwo.id, mockMovieOne.id]
        }
        expect(collectionsFromMovies([mockMovieOne, mockMovieTwo])).toEqual([expectedCollection]);
    });

    it('should put three movies with two different set names in one collection ignoring one', () => {
        const mockMovieOne = getMockMovie("tt1234", 2008, "MockCollectionOne")
        const mockMovieTwo = getMockMovie("tt5678", 2009, "MockCollectionTwo")
        const mockMovieThree = getMockMovie("tt5678", 2009, "MockCollectionTwo")

        const expectedCollection = {
            name: mockMovieTwo.set.name,
            movies: [mockMovieTwo.id, mockMovieThree.id]
        }
        expect(collectionsFromMovies([mockMovieOne, mockMovieTwo, mockMovieThree])).toEqual([expectedCollection]);
    });

    it('should ignore collection if name is on the already existing set name list', () => {
        const collectionName = "mockCollection";
        const mockMovieOne = getMockMovie("tt1234", 2008, "mockCollection")
        const mockMovieTwo = getMockMovie("tt5678", 2009, "mockCollection")

        expect(collectionsFromMovies([mockMovieOne, mockMovieTwo], [collectionName])).toEqual([]);
    });

    function getMockMovie(id, year, setname) {
        return {
            id,
            year,
            set: { name: setname }
        }
    }
});
