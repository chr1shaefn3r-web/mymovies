module.exports = (property, amountOfAllowedItems) =>
    item => {
        return Object.entries(item).reduce((acc, [key, value]) => {
            if (property === key) {
                acc[key] = value.slice(0, amountOfAllowedItems);
            } else {
                acc[key] = value
            }
            return acc;
        }, {});
    }