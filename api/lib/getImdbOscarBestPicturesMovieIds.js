const getImdbMovieIdsFromPage = require('./getImdbMovieIdsFromPage');

module.exports = async () => {
    const linkFromListIdentifier = "ref_=adv_li_tt";
    return await getImdbMovieIdsFromPage("https://www.imdb.com/search/title/?count=100&groups=oscar_best_picture_winners&sort=year,desc", linkFromListIdentifier);
};
