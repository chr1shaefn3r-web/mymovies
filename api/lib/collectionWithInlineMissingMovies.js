const movieDatabase = require("../v1/bff/movies/moviesByProperties")
    (["id", "title", "poster"]);
const compose = require("./compose");
const mapTransducer = require("./mapTransducer");
const lookupIdInMovies = require("./lookIdUpInMovies");

const concat = (a, c) => a.concat(c);

module.exports = (movieIds = []) => {
    const lookupIdInMovieDatabase = mapTransducer(lookupIdInMovies(movieDatabase));
    const xform = compose(
        lookupIdInMovieDatabase
    )(concat);
    const movies = movieIds.reduce(xform, []);
    const amountOfMovies = amountOfFoundMovies(movies);
    const amountOfMissingMovies = movies.length - amountOfMovies;
    const percentage = calculatePercentage(amountOfMissingMovies, movies.length);
    const color = calculateColor(percentage);
    return { percentage, color, amountOfMovies, amountOfMissingMovies, movies }
}

function amountOfFoundMovies(movies) {
    return movies.filter((movie) => movie.poster).length;
}

function calculatePercentage(amountOfMissingMovies, amountOfMovies) {
    return Number((1 - (amountOfMissingMovies / amountOfMovies)) * 100).toFixed();
}

function calculateColor(percentage) {
    const r = Math.floor(255 - percentage * 2.55);
    const g = Math.floor((percentage * 2.55));
    return `#${getHexColor(r)}${getHexColor(g)}00`;
}

function getHexColor(value) {
    let string = value.toString(16);
    return (string.length === 1) ? '0' + string : string;
}