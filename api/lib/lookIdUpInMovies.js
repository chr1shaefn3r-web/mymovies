module.exports = (movies = []) =>
id => {
    const movie = movies.find((item) => item.id === id);
    if (movie) {
        return movie;
    }
    return {
        id
    }
}