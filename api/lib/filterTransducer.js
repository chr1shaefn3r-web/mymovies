module.exports = filter =>
    nextReducer =>
        (accumulator, current) => filter(current) ? nextReducer(accumulator, current) : accumulator;