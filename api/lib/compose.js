module.exports = (...fns) =>
    x => fns.reduceRight((y, f) => f(y), x);