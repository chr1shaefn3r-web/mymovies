const movies = require("../v1/bff/movies/moviesByProperties")
    (["id", "title", "poster"]);
const compose = require("./compose");
const mapTransducer = require("./mapTransducer");
const filterTransducer = require("./filterTransducer");
const isInMovies = require("./isInMovies");
const lookupIdInMovies = require("./lookIdUpInMovies");

const concat = (a, c) => a.concat(c);

module.exports = (favouriteMovieIds = []) => {
    const removeUnknownMovies = filterTransducer(isInMovies(movies));
    const lookupIdInMovieDatabase = mapTransducer(lookupIdInMovies(movies));
    const xform = compose(
        removeUnknownMovies,
        lookupIdInMovieDatabase
    )(concat);
    const favouriteMovies = favouriteMovieIds.reduce(xform, []);
    return { movies: favouriteMovies }
}
