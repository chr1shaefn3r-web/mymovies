const chrome = require('chrome-aws-lambda');
const puppeteer = require('puppeteer-core');

module.exports = async function getImdbMovieIdsFromPage(url, linkFromListIdentifier = "pf_rd_s=center-") {
    const browser = await puppeteer.launch({
        args: chrome.args,
        executablePath: await chrome.executablePath,
        headless: chrome.headless,
    });

    const page = await browser.newPage();
    await page.goto(url);
    const imdbMovieIds = await page.evaluate(parsePageContentForImdbMovieIds, linkFromListIdentifier);
    await browser.close();
    return imdbMovieIds;
}

function parsePageContentForImdbMovieIds(linkFromListIdentifier) {
    const getAllLinks = () => Array.from(document.getElementsByTagName("a"));
    const isImdbTitleLink = (l) => l.startsWith("https://www.imdb.com/title/tt");
    const isLinkFromList = (l) => l.includes(linkFromListIdentifier);
    const extractImdbId = (l) => l.substring(27, 36);
    const isUnique = (v, i, a) => a.indexOf(v) === i;
    return getAllLinks()
        .map(a => a.href)
        .filter(isImdbTitleLink)
        .filter(isLinkFromList)
        .map(extractImdbId)
        .filter(isUnique);
}
