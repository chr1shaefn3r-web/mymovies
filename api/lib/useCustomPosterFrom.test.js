const useCustomPosterFrom = require("./useCustomPosterFrom")

describe("useCustomPosterFrom", () => {

    it('should be defined', () => {
        expect(useCustomPosterFrom).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof useCustomPosterFrom).toBe("function");
    });

    it('should return a "function"', () => {
        expect(typeof useCustomPosterFrom()).toBe("function");
    });

    function getMockMovieWithPoster(poster = "https://example.com/") {
        return {
            id: "tt1234",
            poster
        };
    }
    it('should leave movie untouched if given nothing', () => {
        const transform = useCustomPosterFrom();
        expect(transform(getMockMovieWithPoster())).toEqual(getMockMovieWithPoster());
    });

    it('should use poster from given map', () => {
        const mockMovie = getMockMovieWithPoster();
        const mockPosterUrl = "https://superspecial.com/";
        const mockMap = {
            [mockMovie.id]: mockPosterUrl
        }
        const expectedMovie = getMockMovieWithPoster(mockPosterUrl);
        const transform = useCustomPosterFrom(mockMap);
        expect(transform(mockMovie)).toEqual(expectedMovie);
    });

    function getMockMovie() {
        return { id: "tt1234" }
    }
    it('should only change poster if it already exist', () => {
        const mockMovie = getMockMovie();
        const mockMap = {
            [mockMovie.id]: "https://superspecial.com/"
        }
        const transform = useCustomPosterFrom(mockMap);
        expect(transform(mockMovie)).toEqual(getMockMovie());
    });
});
