module.exports = (movies = [], alreadyExistingSetNames = []) => {
    return sortByKey(movies.reduce((acc, movie) => {
        if (movie.set && movie.set.name && !alreadyExistingSetNames.includes(movie.set.name)) {
            acc[movie.set.name] = acc[movie.set.name] ? acc[movie.set.name].concat([movie]) : [movie]
            acc[movie.set.name] = acc[movie.set.name].sort((firstMovie, secondMovie) => firstMovie.year - secondMovie.year)
        }
        return acc;
    }, {}));
};

function sortByKey(collections) {
    return Object.entries(collections)
        .map(([name, movies]) => {
            if (movies.length <= 1) {
                return;
            }
            return {
                name,
                movies: movies.map(movie => movie.id)
            };
        })
        .filter(Boolean);
}
