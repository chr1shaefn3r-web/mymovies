const allowProperties = require("./allowProperties")

describe("allowProperties", () => {

    it('should be defined', () => {
        expect(allowProperties).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof allowProperties).toBe("function");
    });

    it('should return a "function"', () => {
        expect(typeof allowProperties()).toBe("function");
    });

    it('should remove all properties if no property list is given', () => {
        const transform = allowProperties();
        const inputObject = {a: 1};
        const result = transform(inputObject);
        expect(result).toEqual({});
    });

    it('should remove all properties that are not on the property list', () => {
        const transform = allowProperties(["a"]);
        const inputObject = {a: 1, b: 1};
        const result = transform(inputObject);
        expect(result).toEqual({a: 1});
    });
});
