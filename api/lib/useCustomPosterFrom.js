module.exports = (customPostersMap = {}) =>
    movie => {
        const customPoster = customPostersMap[movie.id];
        if (customPoster && movie.poster) {
            movie.poster = customPoster;
        }
        return movie;
    }