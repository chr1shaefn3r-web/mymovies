const getImdbMovieIdsFromPage = require('./getImdbMovieIdsFromPage');

module.exports = async () => await getImdbMovieIdsFromPage("https://www.imdb.com/chart/top-english-movies");
