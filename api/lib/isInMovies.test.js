const isInMovies = require("./isInMovies")

describe("isInMovies", () => {

    it('should be defined', () => {
        expect(isInMovies).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof isInMovies).toBe("function");
    });

    it('should return a type of "function"', () => {
        expect(typeof isInMovies()).toBe("function");
    });

    it('should return false if no id and no movies', () => {
        expect(isInMovies()()).toBe(false);
    });

    it('should return true if id in movies', () => {
        const mockId = "tt1234"
        const mockMovie = {
            id: mockId
        }
        expect(isInMovies([mockMovie])(mockId)).toBe(true);
    });

    it('should return false if id not in movies', () => {
        const mockMovie = {
            id: "tt1234"
        }
        expect(isInMovies([mockMovie])("tt0000")).toBe(false);
    });
});
