module.exports = (listOfProperties = []) =>
    item => {
        return Object.entries(item).reduce((acc, [key, value]) => {
            if (listOfProperties.includes(key)) {
                acc[key] = value;
            }
            return acc;
        }, {});
    }