const filterTransducer = require("./filterTransducer")

describe("filterTransducer", () => {

    it('should be defined', () => {
        expect(filterTransducer).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof filterTransducer).toBe("function");
    });

    it('should return a type "function"', () => {
        expect(typeof filterTransducer()).toBe("function");
    });

    const doNothing = () => {};
    const inputList = [1,2,3];
    it('should call filter for every item in the list', () => {
        const spy = jest.fn();
        const transducer = filterTransducer(spy);
        const xform = transducer(doNothing);

        inputList.reduce(xform, []);

        expect(spy).toHaveBeenCalledTimes(inputList.length);
    });

    const concat = (accumulator, current) => accumulator.concat(current);
    it('should not change input list if filter always returns true', () => {
        const alwaysTrue = () => true;
        const transducer = filterTransducer(alwaysTrue);
        const xform = transducer(concat);

        const result = inputList.reduce(xform, []);

        expect(result).toEqual(inputList);
    });

    const inputListWithTwoEvanNumbers = [3,4,6];
    const isEven = n => n % 2 === 0;
    it('should call the next reducer for every item in the list that passes the test', () => {
        const spy = jest.fn();
        const transducer = filterTransducer(isEven);
        const xform = transducer(spy);

        inputListWithTwoEvanNumbers.reduce(xform, []);

        expect(spy).toHaveBeenCalledTimes(2);
    });

    it('should return the list of all items that passes the test', () => {
        const transducer = filterTransducer(isEven);
        const xform = transducer(concat);

        const result = inputListWithTwoEvanNumbers.reduce(xform, []);

        expect(result).toEqual([4,6]);
    });
});
