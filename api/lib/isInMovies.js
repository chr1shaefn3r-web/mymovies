module.exports = (movies = []) => {
    const movieIds = movies.map(movie => movie.id)
    return id => movieIds.includes(id)
}