const imdbTop250EngMovies = require("./imdbTop250Eng.json");
const collectionWithInlineMissingMovies = require("../../../lib/collectionWithInlineMissingMovies")

module.exports = () => {
    return collectionWithInlineMissingMovies(imdbTop250EngMovies);
}
