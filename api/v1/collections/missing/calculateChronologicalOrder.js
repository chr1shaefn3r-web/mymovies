const movieLookup = require("./movieLookup")

module.exports = (chronologicalOrder, movies) => {
	if (!chronologicalOrder) {
		return {};
	}
	return chronologicalOrder.reduce((acc, movieId) => {
		const movie = movieLookup.byId(movieId, movies);
		if (movie) {
			acc.chronologicalOrderTitles.push(movie.title)
		}
		return acc;
	}, {
		chronologicalOrderTitles: []
	})
}
