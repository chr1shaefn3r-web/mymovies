const calculateMissingMovies = require("./calculateMissingMovies")

describe("calculateMissingMovies", () => {

	it('should be defined', () => {
		expect(calculateMissingMovies).toBeDefined();
	});

	const movieId1 = "t1234"
	const mockCollectionMovies = [movieId1]
	const mockMovie1 = {
		id: movieId1,
		name: "MockMovie1"
	}
	it('should return found movies of a collection', () => {
		const movies = [mockMovie1];
		expect(calculateMissingMovies(mockCollectionMovies, movies)).toEqual({
			foundMovies: [mockMovie1],
			missingMovies: []
		});
	});

	it('should return missing movies of a collection', () => {
		expect(calculateMissingMovies(mockCollectionMovies, [])).toEqual({
			foundMovies: [],
			missingMovies: [movieId1]
		});
	});
});
