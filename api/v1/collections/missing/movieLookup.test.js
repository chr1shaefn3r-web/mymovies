const { byId } = require("./movieLookup")

describe("movieLookup", () => {

    describe("byId", () => {
        it('should be defined', () => {
            expect(byId).toBeDefined();
        });
        it('should return undefined if nothing given', () => {
            expect(byId()).toBeUndefined();
        });
        const mockId = "t12345";
        const mockMovie = {
            id: mockId,
            name: "mockName"
        }
        it('should return movie object if movie can be found', () => {
            expect(byId(mockId, [mockMovie])).toEqual(mockMovie);
        });
        it('should return undefined if movie can\'t be found', () => {
            const mockMovie = {
                id: "differentId",
                name: "mockName"
            }
            expect(byId(mockId, [mockMovie])).toBeUndefined();
        });
    });

});
