const byId = (movieId, movies = []) => {
    return movies.find((movie) => movie.id === movieId)
};

module.exports = {
    byId
};
