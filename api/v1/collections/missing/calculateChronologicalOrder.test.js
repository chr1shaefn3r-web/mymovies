const calculateChronologicalOrder = require("./calculateChronologicalOrder")

describe("calculateChronologicalOrder", () => {

    it('should be defined', () => {
        expect(calculateChronologicalOrder).toBeDefined();
    });

    const movieId1 = "t1234"
    const mockMovie1 = {
        id: movieId1,
        title: "MockMovie1"
    }
    it('should return empty object if no chronologicalOrder is given', () => {
        const movies = [mockMovie1];
        expect(calculateChronologicalOrder(undefined, movies)).toEqual({});
    });

    const mockChronologicalOrder = [movieId1]
    it('should return title of found movies of the chronologicalOrder', () => {
        const movies = [mockMovie1];
        expect(calculateChronologicalOrder(mockChronologicalOrder, movies)).toEqual({
            chronologicalOrderTitles: [mockMovie1.title]
        });
    });

    it('should ignore unfound movies of the chronologicalOrder', () => {
        const mockChronologicalOrder = ["nonExistingMovieId", movieId1]
        const movies = [mockMovie1];
        expect(calculateChronologicalOrder(mockChronologicalOrder, movies)).toEqual({
            chronologicalOrderTitles: [mockMovie1.title]
        });
    });
});
