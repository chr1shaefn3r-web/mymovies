const calculator = require("./calculateMissingMovies")
const calculateChronologicalOrder = require("./calculateChronologicalOrder")
const allCollections = require("../../collections/allCollections")
const movies = require("../../bff/movies/moviesByProperties")
    (["id", "file", "title", "poster"]);

module.exports = () => {
  return allCollections().map((collection) => {
    return Object.assign({}, collection, calculator(collection.movies, movies), calculateChronologicalOrder(collection.chronologicalOrder, movies));
  })
}
