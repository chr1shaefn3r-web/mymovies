const movieLookup = require("./movieLookup")

module.exports = (mockCollectionMovies, movies) => {
	return mockCollectionMovies.reduce((acc, movieId) => {
		const movie = movieLookup.byId(movieId, movies);
		if (movie) {
			acc.foundMovies.push(movie)
		} else {
			acc.missingMovies.push(movieId)
		}
		return acc;
	}, {
			foundMovies: [],
			missingMovies: []
		})
}
