const imdbTop250Movies = require("./imdbTop250.json")
const collectionWithInlineMissingMovies = require("../../../lib/collectionWithInlineMissingMovies")

module.exports = () => {
    return collectionWithInlineMissingMovies(imdbTop250Movies);
}
