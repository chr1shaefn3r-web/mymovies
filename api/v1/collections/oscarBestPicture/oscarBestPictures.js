const interestingOscarBestPictures = require("./interestingOscarBestPictures")
const collectionWithInlineMissingMovies = require("../../../lib/collectionWithInlineMissingMovies")

module.exports = () => {
    return collectionWithInlineMissingMovies(interestingOscarBestPictures());
}
