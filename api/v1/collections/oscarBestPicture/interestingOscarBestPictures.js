const oscarBestPictureMovies = require("./oscarBestPicture.json")

module.exports = () => {
    const imdbIdGodfather = "tt0068646"; // Not interested in movies older than The Godfather (1972)
    const indexOfBenHur = oscarBestPictureMovies.indexOf(imdbIdGodfather);
    const oscarBestPictureMoviesStartingFromBenHur = oscarBestPictureMovies.slice(0, indexOfBenHur+1);
    return oscarBestPictureMoviesStartingFromBenHur;
}
