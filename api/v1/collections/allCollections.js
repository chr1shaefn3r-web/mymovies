const customCollection = require("./custom.json")
const customCollectionNames = customCollection.map(collection => collection.name)
const collectionsFromMovies = require("../../lib/collectionsFromMovies")
const movies = require("../bff/movies/moviesByProperties")
    (["id", "set", "year"]);

module.exports = () => collectionsFromMovies(movies, customCollectionNames)
    .concat(customCollection);

