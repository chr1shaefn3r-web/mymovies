const allCollections = require("../../collections/allCollections")
const stylizedCollections = require("./stylizedCollections.json");
const movies = require("../../bff/movies/moviesByProperties")
    (["id", "title", "poster"]);
const movieLookup = require("../missing/movieLookup")
const useCustomPosterFrom = require("../../../lib/useCustomPosterFrom");
const stylizedMovieposter = require("./stylizedMovieposter.json");

module.exports = () => {
    return allCollections()
        .filter((collection) => stylizedCollections.includes(collection.name))
        .map((collection) => {
            const collectionMovies = collection.movies.reduce((acc, movieId) => {
                const movie = movieLookup.byId(movieId, movies);
                if (!movie) {
                    return acc;
                }
                return acc.concat(movie);
            }, []);
            return {
                name: collection.name,
                movies: collectionMovies.map(useCustomPosterFrom(stylizedMovieposter))
            }
        })
}
