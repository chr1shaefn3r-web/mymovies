const missing = require("../../collections/missing/missing")
const sortByAmountOfMissingMovies = require("./sortByAmountOfMissingMovies")
const filterOutFinishedCollections = require("./filterOutFinishedCollections")

module.exports = (limit) => {
    const unfinishedCollections = filterOutFinishedCollections(missing());
    return sortByAmountOfMissingMovies(unfinishedCollections).slice(0, limit)
}
