const sortByDateadded = require("./sortByDateadded")

describe("sortByDateadded", () => {

    it('should be defined', () => {
        expect(sortByDateadded).toBeDefined();
    });

    it('should return an empty array if given nothing', () => {
        expect(sortByDateadded()).toEqual(expect.any(Array));
    });

    const mockMovieWithVeryOldDateAdded = {
        title: "Matrix",
        dateadded: "2014-05-28 22:03:35"
    };
    const mockMovieWithCurrentDateAdded = {
        title: "Herr der Ringe",
        dateadded: "2019-05-28 12:00:00"
    };
    it('should sort two movies by date', () => {
        expect(sortByDateadded([mockMovieWithVeryOldDateAdded, mockMovieWithCurrentDateAdded])).toEqual([
            mockMovieWithCurrentDateAdded, mockMovieWithVeryOldDateAdded
        ]);
    });
    it('should not change sorted list', () => {
        expect(sortByDateadded([mockMovieWithCurrentDateAdded, mockMovieWithVeryOldDateAdded])).toEqual([
            mockMovieWithCurrentDateAdded, mockMovieWithVeryOldDateAdded
        ]);
    });
});
