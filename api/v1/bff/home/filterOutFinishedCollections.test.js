const filterOutFinishedCollections = require("./filterOutFinishedCollections")

describe("filterOutFinishedCollections", () => {

	it('should be defined', () => {
		expect(filterOutFinishedCollections).toBeDefined();
	});


    it('should return an empty array if given nothing', () => {
        expect(filterOutFinishedCollections()).toEqual(expect.any(Array));
    });

    const mockMissingCollectionWithOneMissingMovie = {
        name: "300",
        missingMovies: ["tt6"]
    };
    const mockMissingCollectionWithoutMissingMovies = {
        name: "Kill Bill",
        missingMovies: []
    };
    it('should filter out missing collections without missing movies', () => {
        expect(filterOutFinishedCollections([mockMissingCollectionWithOneMissingMovie, mockMissingCollectionWithoutMissingMovies])).toEqual([
            mockMissingCollectionWithOneMissingMovie
        ]);
    });
    it('should not change filtered list', () => {
        expect(filterOutFinishedCollections([mockMissingCollectionWithOneMissingMovie])).toEqual([
            mockMissingCollectionWithOneMissingMovie
        ]);
    });
});
