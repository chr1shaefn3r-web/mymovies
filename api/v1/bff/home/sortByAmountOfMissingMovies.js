module.exports = (missingCollections = []) => {
    return missingCollections.sort((firstMissingCollection, secondMissingCollection) => {
        return firstMissingCollection.missingMovies.length - secondMissingCollection.missingMovies.length;
    });
}