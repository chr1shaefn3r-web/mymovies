const sortByDateadded = require("./sortByDateadded");
const movies = require("../movies/moviesByProperties")
    (["id", "title", "poster", "dateadded"]);

module.exports = (limit) => {
    return sortByDateadded(movies).slice(0, limit);
}