const sortByAmountOfMissingMovies = require("./sortByAmountOfMissingMovies")

describe("sortByAmountOfMissingMovies", () => {

	it('should be defined', () => {
		expect(sortByAmountOfMissingMovies).toBeDefined();
	});


    it('should return an empty array if given nothing', () => {
        expect(sortByAmountOfMissingMovies()).toEqual(expect.any(Array));
    });

    const mockMissingCollectionWithALotOfMissingMovies = {
        name: "James Bond",
        missingMovies: ["tt1", "tt2", "tt3", "tt4", "tt5"]
    };
    const mockMissingCollectionWithOneMissingMovie = {
        name: "Kill Bill",
        missingMovies: ["tt6"]
    };
    const mockMissingCollectionWithTwoMissingMovies = {
        name: "Ant-Man",
        missingMovies: ["tt6", "tt7"]
    };
    it('should sort two missing collections by amount of missing movies (one vs two)', () => {
        expect(sortByAmountOfMissingMovies([mockMissingCollectionWithTwoMissingMovies, mockMissingCollectionWithOneMissingMovie])).toEqual([
            mockMissingCollectionWithOneMissingMovie, mockMissingCollectionWithTwoMissingMovies
        ]);
    });
    it('should sort two missing collections by amount of missing movies (one vs many)', () => {
        expect(sortByAmountOfMissingMovies([mockMissingCollectionWithALotOfMissingMovies, mockMissingCollectionWithOneMissingMovie])).toEqual([
            mockMissingCollectionWithOneMissingMovie, mockMissingCollectionWithALotOfMissingMovies
        ]);
    });
    it('should not change sorted list', () => {
        expect(sortByAmountOfMissingMovies([mockMissingCollectionWithOneMissingMovie, mockMissingCollectionWithALotOfMissingMovies])).toEqual([
            mockMissingCollectionWithOneMissingMovie, mockMissingCollectionWithALotOfMissingMovies
        ]);
    });
});
