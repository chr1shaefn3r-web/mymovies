const interestingOscarBestPictures = require("../../collections/oscarBestPicture/interestingOscarBestPictures");
const imdbTop250Eng = require("../../collections/imdbTop250Eng/imdbTop250Eng.json");
const movieIdDatabase = require("../movies/moviesByProperties")(["id"]).map(movie => movie.id);

module.exports = () => {
    return interestingOscarBestPictures()
        .filter(id => !movieIdDatabase.includes(id))
        .filter((oscarBestPictureMovieId) => imdbTop250Eng.includes(oscarBestPictureMovieId));
};
