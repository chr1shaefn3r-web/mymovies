module.exports = (missingCollections = []) => {
    return missingCollections.filter((missingCollection) => missingCollection.missingMovies.length)
}