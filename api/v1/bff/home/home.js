const lastAddedMovies = require("./lastAddedMovies")
const closeToCompletedCollections = require("./closeToCompletedCollections")
const missingImdbTop250EngOscarBestPictureWinners = require("./missingImdbTop250EngOscarBestPictureWinners")

module.exports = () => {
    return {
        lastAddedMovies: lastAddedMovies(6),
        closeToCompletedCollections: closeToCompletedCollections(4),
        missingImdbTop250EngOscarBestPictureWinners: missingImdbTop250EngOscarBestPictureWinners()
    };
}
