const moviesWithoutCollection = require('./moviesWithoutCollection');
const sortAlphabetically = require('./sortAlphabetically');

module.exports = (movies = [], collections = []) => {
    if (collections.length === 0) {
        return movies;
    }

    const moviesNotInACollection = moviesWithoutCollection(movies, collections);
    const virtualCollectionForMoviesWithoutOne = moviesNotInACollection.map((movie) => {
        return {
            name: movie.title,
            movies: [movie]
        }
    })

    const collectionsForMoviesWithOne = collections.map((collection) => {
        collection.movies = collection.movies
            .map((movieId => movies.find((movie) => movie.id === movieId)))
            .filter(Boolean);
        return collection;
    }, []);

    const sortedCollections = sortAlphabetically(collectionsForMoviesWithOne.concat(virtualCollectionForMoviesWithoutOne), "name")
    return sortedCollections.reduce((acc, collection) => {
        return acc.concat(collection.movies);
    }, []);
}