const movies = require("../../movies/data.json");
const myShelfMoviePosters = require("../../movies/myshelfmovieposters.json");
const compose = require("../../../lib/compose");
const concat = require("../../../lib/concat");
const allowProperties = require("../../../lib/allowProperties");
const reduceAmountOfItemsInProperty = require("../../../lib/reduceAmountOfItemsInProperty");
const useCustomPosterFrom = require("../../../lib/useCustomPosterFrom");
const mapTransducer = require("../../../lib/mapTransducer");
const { significantActors, mainGenres } = require("../../config")

module.exports = (neededProperties) => {
    const removeUnneededProperties = mapTransducer(allowProperties(neededProperties));
    const shelfMoviePosters = mapTransducer(useCustomPosterFrom(myShelfMoviePosters));
    const onlySignifacantActors = mapTransducer(reduceAmountOfItemsInProperty("actors", significantActors));
    const onlyMainGenres = mapTransducer(reduceAmountOfItemsInProperty("genre", mainGenres));
    const xform = compose(
        removeUnneededProperties,
        shelfMoviePosters,
        onlySignifacantActors,
        onlyMainGenres
    )(concat);

    return movies.reduce(xform, []);
}
