const moviesWithoutCollection = require("./moviesWithoutCollection")

describe("moviesWithoutCollection", () => {

    it('should be defined', () => {
        expect(moviesWithoutCollection).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof moviesWithoutCollection).toBe("function");
    });

    it('should return an empty array if given nothing', () => {
        expect(moviesWithoutCollection()).toEqual([]);
    });

    const mockMovieOne = {
        id: "tt1"
    }
    it('should return all movies if not given any collections', () => {
        const actual = moviesWithoutCollection([mockMovieOne]);
        const expected = [mockMovieOne];
        expect(actual).toEqual(expected);
    });
    const mockCollectionWithoutMovies = {
        movies: []
    }
    it('should return all movies if given collection without movies', () => {
        const actual = moviesWithoutCollection([mockMovieOne], [mockCollectionWithoutMovies]);
        const expected = [mockMovieOne];
        expect(actual).toEqual(expected);
    });
    const mockCollectionWithAllMovies = {
        movies: [mockMovieOne.id]
    }
    it('should return no movies if given collection with all movies', () => {
        const actual = moviesWithoutCollection([mockMovieOne], [mockCollectionWithAllMovies]);
        const expected = [];
        expect(actual).toEqual(expected);
    });
    it('should return no movies if given two collections one with all movies', () => {
        const actual = moviesWithoutCollection([mockMovieOne], [mockCollectionWithoutMovies, mockCollectionWithAllMovies]);
        const expected = [];
        expect(actual).toEqual(expected);
    });
});
