const sortAlphabetically = require("./sortAlphabetically");

describe("sortAlphabetically", () => {

	it('should return an array', () => {
		expect(Array.isArray(sortAlphabetically())).toBe(true);
		expect(Array.isArray(sortAlphabetically({}))).toBe(true);
		expect(Array.isArray(sortAlphabetically([]))).toBe(true);
	});

	const propertyName = "title";
	const getSortedMockMovieList = () => [
		{ [propertyName]: '96 Hours - Taken 2 (2012)' },
		{ [propertyName]: 'American Hustle' },
		{ [propertyName]: 'Men in Black (1997)' },
		{ [propertyName]: 'Moneyball - Die Kunst zu gewinnen' }
	];
	it('should not change a already sorted list', () => {
		const expected = getSortedMockMovieList();
		const actual = sortAlphabetically(getSortedMockMovieList(), propertyName);
		expect(actual).toEqual(expected);
	});

	it('should sort a list', () => {
		const mockUnsortedMovieList = [
			{ [propertyName]: 'Men in Black (1997)' },
			{ [propertyName]: '96 Hours - Taken 2 (2012)' },
			{ [propertyName]: 'Moneyball - Die Kunst zu gewinnen' },
			{ [propertyName]: 'American Hustle' }
		];
		const expected = getSortedMockMovieList();
		expect(sortAlphabetically(mockUnsortedMovieList, propertyName)).toEqual(expected);
	});

	const getSortedMockMovieListWithDuplicate = () => [
		{ [propertyName]: 'American Hustle' },
		{ [propertyName]: 'American Hustle' }
	]
	it('should sort a list', () => {
		const expected = getSortedMockMovieListWithDuplicate();
		const actual = sortAlphabetically(getSortedMockMovieListWithDuplicate(), propertyName);
		expect(actual).toEqual(expected);
	});

});
