const moviesByProperties = require("./moviesByProperties");
const collectionsFromMovies = require("../../../lib/collectionsFromMovies");
const movies = require("../../movies/data.json");
const sortByCollections = require("./sortByCollections");

module.exports = () => {
    return sortByCollections(
        moviesByProperties(["id", "file", "title", "poster", "tagline", "year", "plot", "genre", "actors", "runtime", "mediaType", "director"]),
        collectionsFromMovies(movies)
    );
}
