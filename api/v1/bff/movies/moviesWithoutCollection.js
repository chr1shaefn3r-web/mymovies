module.exports = (movies = [], collections = []) =>
    movies.filter(isPartOfACollection(collections))

const isPartOfACollection = (collections) =>
    // If there are no collections, there is no way to judge
    // the movie good to go
    movie => {
        if (collections.length === 0) {
            return true;
        }
        return collections.every(movieIsPartOfTheCollection(movie))
    }

const movieIsPartOfTheCollection = (movie) =>
    collection => {
        return !collection.movies.includes(movie.id)
    }