const sortByCollections = require("./sortByCollections")

describe("sortByCollections", () => {

    it('should be defined', () => {
        expect(sortByCollections).toBeDefined();
    });

    it('should be of type "function"', () => {
        expect(typeof sortByCollections).toBe("function");
    });

    it('should return empty array if given nothing', () => {
        expect(sortByCollections()).toEqual([]);
    });

    const mockLotR1 = {
        id: "tt0120737",
        title: "Der Herr der Ringe - Die Gefährten"
    };
    const mockLotR2 = {
        id: "tt0167261",
        title: "Der Herr der Ringe - Die zwei Türme"
    };
    const mockLotR3 = {
        id: "tt0167260",
        title: "Der Herr der Ringe - Die Rückkehr des Königs"
    };
    const mockLotRMoviesSortedAlphabetically = [mockLotR1, mockLotR3, mockLotR2] // sorted alphabetically

    it('should return movies unchanged if there are no collections', () => {
        expect(sortByCollections(mockLotRMoviesSortedAlphabetically)).toEqual(mockLotRMoviesSortedAlphabetically);
    });

    it('should sort by movies by order in their collection', () => {
        const mockLotRCollection = {
            id: "119",
            name: "Herr der Ringe",
            movies: [
                "tt0120737",
                "tt0167261",
                "tt0167260"
            ]
        }
        const expected = [mockLotR1, mockLotR2, mockLotR3]
        expect(sortByCollections(mockLotRMoviesSortedAlphabetically, [mockLotRCollection])).toEqual(expected);
    });

    it('should ignore movies in collection which are not in movies', () => {
        const mockZombieland1 = {
            id: "tt1156398",
            title: "Zombieland"
        };
        const mockZombielandCollection = {
            id: "537982",
            name: "Zombieland",
            movies: [
                "tt1156398",
                "tt1560220"
            ]
        }
        expect(sortByCollections([mockZombieland1], [mockZombielandCollection])).toEqual([mockZombieland1]);
    });

    it('should sort by collection name alphabetically', () => {
        const mockPotC1 = {
            id: "tt0325980",
            title: "Fluch der Karibik"
        };
        const mockPotC2 = {
            id: "tt0383574",
            title: "Pirates of the Caribbean - Fluch der Karibik 2"
        };
        const mockFifthElement = {
            id: "tt0119116",
            title: "Das fünfte Element"
        };
        const mockMoviesSortedAlphabetically = [mockPotC1, mockFifthElement, mockPotC2];
        const mockPotCCollection = {
            id: "295",
            name: "Fluch der Karibik",
            movies: [
                "tt0325980",
                "tt0383574"
            ]
        }
        const expected = [mockPotC1, mockPotC2, mockFifthElement]
        expect(sortByCollections(mockMoviesSortedAlphabetically, [mockPotCCollection])).toEqual(expected);
    });
});
