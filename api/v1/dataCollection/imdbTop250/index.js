const getImdbTop250MovieIds = require('../../../lib/getImdbTop250MovieIds');
const answerWithJsonString = require('../../../lib/answerWithJsonString');

module.exports = async (req, res) => answerWithJsonString(res, await getImdbTop250MovieIds());
