const getImdbOscarBestPicturesMovieIds = require('../../../lib/getImdbOscarBestPicturesMovieIds');
const answerWithJsonString = require('../../../lib/answerWithJsonString');

module.exports = async (req, res) => answerWithJsonString(res, await getImdbOscarBestPicturesMovieIds());
