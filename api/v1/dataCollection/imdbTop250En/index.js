const getImdbTop250EnMovieIds = require('../../../lib/getImdbTop250EnMovieIds');
const answerWithJsonString = require('../../../lib/answerWithJsonString');

module.exports = async (req, res) => answerWithJsonString(res, await getImdbTop250EnMovieIds());
