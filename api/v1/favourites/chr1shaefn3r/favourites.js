const favouriteMovieIds = require("./data.json")
const favourites = require("../../../lib/favourites")

module.exports = () => {
    return favourites(favouriteMovieIds)
}
