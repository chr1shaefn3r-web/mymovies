const watchedMovies = require("./watched.json")
const collectionWithInlineMissingMovies = require("../../../lib/collectionWithInlineMissingMovies")

module.exports = () => {
    return collectionWithInlineMissingMovies(watchedMovies);
}
