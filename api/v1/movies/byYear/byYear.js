const compose = require("../../../lib/compose");
const concat = require("../../../lib/concat");
const mapTransducer = require("../../../lib/mapTransducer");
const allowProperties = require("../../../lib/allowProperties");
const mapToByYear = require("./mapToByYear");
const sortByKey = require("./sortByKey");
const movies = require("../data.json")

module.exports = () => {
    const removeUnneededProperties = mapTransducer(allowProperties(["id", "file", "title", "poster", "year"]));
    const xform = compose(
        removeUnneededProperties
    )(concat);
    const reducedMovies = movies.reduce(xform, []);
    return sortByKey(mapToByYear(reducedMovies));
}
