module.exports = (movies = {}) => {
    return Object.entries(movies)
    .sort(([firstKey], [secondKey]) => secondKey - firstKey)
    .map(([year, movies]) => {
        return {year, movies};
    });
}
