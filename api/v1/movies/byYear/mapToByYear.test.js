const mapToByYear = require("./mapToByYear")

describe("mapToByYear", () => {

    it('should be defined', () => {
        expect(mapToByYear).toBeDefined();
    });

    it('should return an empty object if given nothing', () => {
        expect(mapToByYear()).toEqual(expect.any(Object));
    });

    it('should return an empty object if given empty array', () => {
        expect(mapToByYear([])).toEqual(expect.any(Object));
    });

    const mockMovieOneFrom2003 = {
        title: "Matrix",
        year: "2003"
    };
    const mockMovieTwoFrom2003 = {
        title: "Johnny English",
        year: "2003"
    };
    it('should map two movies to their respective year', () => {
        expect(mapToByYear([mockMovieOneFrom2003, mockMovieTwoFrom2003])).toEqual({
            2003: [mockMovieOneFrom2003, mockMovieTwoFrom2003]
        });
    });

    const mockMovieOneFrom2001 = {
        title: "Herr der Ringe",
        year: "2001"
    };
    it('should map two movies from different years to their respective year', () => {
        expect(mapToByYear([mockMovieOneFrom2003, mockMovieOneFrom2001])).toEqual({
            2003: [mockMovieOneFrom2003],
            2001: [mockMovieOneFrom2001]
        });
    });
});
