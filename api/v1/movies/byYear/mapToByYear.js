module.exports = (movies = []) => {
    return movies.reduce((acc, value) => {
        acc[value.year] = acc[value.year] ? acc[value.year].concat(value) : acc[value.year] = [value];
        return acc;
    }, {});
}