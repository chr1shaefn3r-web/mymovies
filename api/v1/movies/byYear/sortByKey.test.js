const sortByKey = require("./sortByKey")

describe("sortByKey", () => {

    it('should be defined', () => {
        expect(sortByKey).toBeDefined();
    });

    it('should return an empty array if given nothing', () => {
        expect(sortByKey()).toEqual(expect.any(Array));
    });

    it('should return an empty array if given empty array', () => {
        expect(sortByKey([])).toEqual(expect.any(Array));
    });

    it('should not change an already sorted object', () => {
        const mockMoviesByYear = {
            2018: []
        };
        expect(sortByKey(mockMoviesByYear)).toEqual([{year: "2018", movies: []}]);
    });

    it('should not change an already sorted object - with movie', () => {
        const mockMoviesByYearWithMovie = {
            2003: [{
                title: "Matrix",
                year: "2003"
            }]
        };
        expect(sortByKey(mockMoviesByYearWithMovie)).toEqual([{year: "2003", movies: [{
            title: "Matrix",
            year: "2003"
        }]}]);
    });

    it('should sort by year', () => {
        const mockMoviesByYearWithMovie = {
            1992: [],
            2003: []
        };
        expect(sortByKey(mockMoviesByYearWithMovie)).toEqual([{
            year: "2003", movies: []
        }, {
            year: "1992", movies: []
        }]);
    });
});
