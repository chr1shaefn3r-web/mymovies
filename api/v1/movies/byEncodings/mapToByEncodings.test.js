const mapToByEncodings = require("./mapToByEncodings")

describe("mapToByMediaType", () => {

    it('should be defined', () => {
        expect(mapToByEncodings).toBeDefined();
    });

    it('should return an empty object if given nothing', () => {
        expect(mapToByEncodings()).toEqual(expect.any(Object));
    });

    it('should return an empty object if given empty array', () => {
        expect(mapToByEncodings([])).toEqual(expect.any(Object));
    });

    const mockMovieOne = {
        id: "ttOne"
    };
    it('should sort movie into its encoding', () => {
        const mockEncodingWithMovieOne = {
            name: "h264",
            fileSizeMap: {
                [mockMovieOne.id]: 1234
            }
        };
        const expected = {
            [mockEncodingWithMovieOne.name]: [mockMovieOne]
        }
        const actual = mapToByEncodings([mockMovieOne], [mockEncodingWithMovieOne]);
        expect(actual).toEqual(expected);
    });

    const mockMovieTwo = {
        id: "ttTwo"
    };
    it('should sort two movies into their encoding', () => {
        const mockEncodingWithMovieOneAndTwo = {
            name: "h264",
            fileSizeMap: {
                [mockMovieOne.id]: 1234,
                [mockMovieTwo.id]: 5678
            }
        };
        const expected = {
            [mockEncodingWithMovieOneAndTwo.name]: [mockMovieOne, mockMovieTwo]
        }
        const actual = mapToByEncodings([mockMovieOne, mockMovieTwo], [mockEncodingWithMovieOneAndTwo]);
        expect(actual).toEqual(expected);
    });

    it('should ignore encodings without movies', () => {
        const mockEncodingOneWithMovieOne = {
            name: "h264",
            fileSizeMap: {
                [mockMovieOne.id]: 1234
            }
        };
        const mockEncodingTwoWithoutMovies = {
            name: "h265",
            fileSizeMap: {}
        };
        const expected = {
            [mockEncodingOneWithMovieOne.name]: [mockMovieOne]
        }
        const actual = mapToByEncodings([mockMovieOne], [mockEncodingOneWithMovieOne, mockEncodingTwoWithoutMovies]);
        expect(actual).toEqual(expected);
    });

    it('should add single movie into multiple encodings', () => {
        const mockEncodingOneWithMovieOne = {
            name: "h264",
            fileSizeMap: {
                [mockMovieOne.id]: 1234
            }
        };
        const mockEncodingTwoWithMovieTwo = {
            name: "h265",
            fileSizeMap: {
                [mockMovieOne.id]: 5678
            }
        };
        const expected = {
            [mockEncodingOneWithMovieOne.name]: [mockMovieOne],
            [mockEncodingTwoWithMovieTwo.name]: [mockMovieOne]
        }
        const actual = mapToByEncodings([mockMovieOne], [mockEncodingOneWithMovieOne, mockEncodingTwoWithMovieTwo]);
        expect(actual).toEqual(expected);
    });
});
