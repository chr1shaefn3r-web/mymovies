module.exports = (movies = {}) => {
    return Object.entries(movies)
    .sort(([firstKey], [secondKey]) => firstKey.localeCompare(secondKey))
    .map(([encoding, movies]) => {
        return {encoding, movies};
    });
}
