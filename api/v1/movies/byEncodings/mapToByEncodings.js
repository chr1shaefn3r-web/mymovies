module.exports = (movies = [], encodings = []) => {
    return movies.reduce((acc, movie) => {
        encodings.forEach((encoding) => {
            if (encoding.fileSizeMap[movie.id]) {
                acc[encoding.name] = acc[encoding.name] ? acc[encoding.name].concat(movie) : acc[encoding.name] = [movie];
            }
        })
        return acc;
    }, {});
}