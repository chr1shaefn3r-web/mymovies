const compose = require("../../../lib/compose");
const concat = require("../../../lib/concat");
const mapTransducer = require("../../../lib/mapTransducer");
const allowProperties = require("../../../lib/allowProperties");
const mapToByMediaType = require("./mapToByEncodings");
const sortByKey = require("./sortByKey");
const movies = require("../data.json")


const encodings = [{
    name: "h264",
    fileSizeMap: require("../../../v1/movies/movieFilesizeMap.json")
}, {
    name: "h265",
    fileSizeMap: require("../../../v1/movies/movieFilesizeMap.h265.json")
}]

module.exports = () => {
    const removeUnneededProperties = mapTransducer(allowProperties(["id", "file", "title", "poster", "mediaType"]));
    const xform = compose(
        removeUnneededProperties
    )(concat);
    const reducedMovies = movies.reduce(xform, []);
    return sortByKey(mapToByMediaType(reducedMovies, encodings));
}
