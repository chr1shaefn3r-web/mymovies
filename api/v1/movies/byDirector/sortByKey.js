const directors = require("../directors.json");

module.exports = (movies = {}) => {
    return Object.entries(movies)
    .sort(([firstKey,], [secondKey]) => firstKey.localeCompare(secondKey))
    .map(([director, movies]) => {
        const directorMetadata = directors[director] || {};
        return {
            director,
            movies,
            picture: directorMetadata.picture,
            linkToDirectorMovieDbPage: `https://www.themoviedb.org/person/${directorMetadata.id}`
        };
    });
}
