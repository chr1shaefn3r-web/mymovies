const compose = require("../../../lib/compose");
const concat = require("../../../lib/concat");
const mapTransducer = require("../../../lib/mapTransducer");
const allowProperties = require("../../../lib/allowProperties");
const mapToByDirector = require("./mapToByDirector");
const sortByKey = require("./sortByKey");
const movies = require("../data.json")

const atLeastTwoMovies = ({ movies }) => movies.length >= 2;

module.exports = () => {
    const removeUnneededProperties = mapTransducer(allowProperties(["id", "file", "title", "poster", "director", "year"]));
    const xform = compose(
        removeUnneededProperties
    )(concat);
    const reducedMovies = movies.reduce(xform, []);
    const sortedArray = sortByKey(mapToByDirector(reducedMovies));
    return sortedArray
        .filter(atLeastTwoMovies);
}
