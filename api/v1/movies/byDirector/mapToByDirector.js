module.exports = (movies = []) => {
    return movies.reduce((acc, value) => {
        acc[value.director] = acc[value.director] ? acc[value.director].concat(value) : acc[value.director] = [value];
        return acc;
    }, {});
}