const compose = require("../../../lib/compose");
const concat = require("../../../lib/concat");
const mapTransducer = require("../../../lib/mapTransducer");
const allowProperties = require("../../../lib/allowProperties");
const mapToByMediaType = require("./mapToByMediaType");
const sortByKey = require("./sortByKey");
const movies = require("../data.json")
const uhdRawFileSizeMap = require("../../../v1/movies/movieFilesizeMap.uhd.raw.json");

module.exports = (selectedMediaType) => {
    const removeUnneededProperties = mapTransducer(allowProperties(["id", "file", "title", "poster", "mediaType"]));
    const xform = compose(
        removeUnneededProperties
    )(concat);
    const reducedMovies = movies.reduce(xform, []);
    const result = sortByKey(mapToByMediaType(reducedMovies));
    const uhdRawMovies = reducedMovies.filter((movie) => !!uhdRawFileSizeMap[movie.id]);
    result.push({mediaType: 'uhd.raw', movies: uhdRawMovies})
    if (!selectedMediaType) {
        return result;
    }
    return result.filter(media => media.mediaType === selectedMediaType);
}
