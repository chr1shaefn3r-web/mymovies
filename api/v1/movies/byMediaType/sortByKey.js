module.exports = (movies = {}) => {
    return Object.entries(movies)
    .sort(([firstKey], [secondKey]) => secondKey - firstKey)
    .map(([mediaType, movies]) => {
        return {mediaType, movies};
    });
}
