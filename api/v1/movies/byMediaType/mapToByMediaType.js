module.exports = (movies = []) => {
    return movies.reduce((acc, value) => {
        acc[value.mediaType] = acc[value.mediaType] ? acc[value.mediaType].concat(value) : acc[value.mediaType] = [value];
        return acc;
    }, {});
}