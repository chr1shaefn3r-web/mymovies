const mapToByMediaType = require("./mapToByMediaType")

describe("mapToByMediaType", () => {

    it('should be defined', () => {
        expect(mapToByMediaType).toBeDefined();
    });

    it('should return an empty object if given nothing', () => {
        expect(mapToByMediaType()).toEqual(expect.any(Object));
    });

    it('should return an empty object if given empty array', () => {
        expect(mapToByMediaType([])).toEqual(expect.any(Object));
    });

    const mockMovieOneFromDvd = {
        title: "Matrix",
        mediaType: "dvd"
    };
    const mockMovieTwoFromDvd = {
        title: "Johnny English",
        mediaType: "dvd"
    };
    it('should map two movies to their respective year', () => {
        expect(mapToByMediaType([mockMovieOneFromDvd, mockMovieTwoFromDvd])).toEqual({
            "dvd": [mockMovieOneFromDvd, mockMovieTwoFromDvd]
        });
    });

    const mockMovieOneFromBluRay = {
        title: "Herr der Ringe",
        mediaType: "bluray"
    };
    it('should map two movies from different years to their respective year', () => {
        expect(mapToByMediaType([mockMovieOneFromDvd, mockMovieOneFromBluRay])).toEqual({
            "dvd": [mockMovieOneFromDvd],
            "bluray": [mockMovieOneFromBluRay]
        });
    });
});
