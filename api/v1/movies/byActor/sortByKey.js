module.exports = (movies = {}) => {
    return Object.entries(movies)
        .sort(([firstKey], [secondKey]) => firstKey.localeCompare(secondKey))
        .map(([actor, values]) => {
            values.movies.forEach((movie) => delete movie.actors)
            return { actor, ...values };
        });
}
