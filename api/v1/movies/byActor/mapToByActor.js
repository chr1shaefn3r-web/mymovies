module.exports = (acc, movie) => {
    if(!movie.actors) {
        return acc;
    }
    movie.actors
        .forEach((actor) => {
            acc[actor.name] = appendMovie(acc[actor.name], actor, movie);
        });
    return acc;
}

function appendMovie(old, actor, movie) {
    if (old) {
        return Object.assign(old, {
            movies: old.movies.concat(movie)
        });
    }
    return {
        movies: [movie],
        thumb: actor.thumb
    }
}