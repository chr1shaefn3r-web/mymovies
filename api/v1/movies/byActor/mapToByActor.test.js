const mapToByActor = require("./mapToByActor")

describe("mapToByActor", () => {

    it('should be defined', () => {
        expect(mapToByActor).toBeDefined();
    });

    it('should ignore movies without actors', () => {
        expect(mapToByActor({}, movieWithoutActors())).toEqual({});
    });

    const firstActor = {
        name: "Keanu Revees",
        role: "Neo",
        thumb: "http://image.tmdb.org/t/p/original/bOlYWhVuOiU6azC4Bw6zlXZ5QTC.jpg"
    };
    it('should return a object with one key if given a movie with one actor', () => {
        expect(mapToByActor({}, secondMovieWithActors([firstActor]))).toEqual({
            ["Keanu Revees"]: {
                movies: [secondMovieWithActors([firstActor])],
                thumb: firstActor.thumb
            }
        });
    });

    const secondActor = {
        name: "Laurence Fishburne",
        role: "Morpheus",
        thumb: "http://image.tmdb.org/t/p/original/8suOhUmPbfKqDQ17jQ1Gy0mI3P4.jpg"
    }
    it('should return a object with two keys if given a movie with two actors', () => {
        const movie = firstMovieWithActors([firstActor, secondActor]);
        const movies = [movie];
        expect(mapToByActor({}, movie)).toEqual({
            ["Keanu Revees"]: {
                movies,
                thumb: firstActor.thumb
            },
            ["Laurence Fishburne"]: {
                movies,
                thumb: secondActor.thumb
            }
        });
    });
    it('should return a object with two keys if given a movie with two actors', () => {
        const firstMovie = firstMovieWithActors([firstActor]);
        const secondMovie = secondMovieWithActors([firstActor]);
        const movies = [firstMovie];
        expect(mapToByActor({
            ["Keanu Revees"]: {
                movies,
                thumb: firstActor.thumb
            }
        }, secondMovie))
            .toEqual({
                ["Keanu Revees"]: {
                    movies: movies.concat(secondMovie),
                    thumb: firstActor.thumb
                }
            });
    });
});

function movieWithoutActors() {
    return {
        title: "BlacKkKlansman",
        poster: "https://image.tmdb.org/t/p/original/n7bMWlDNTF8RHiR4SF18174EmF7.jpg",
    }
}

function firstMovieWithActors(actors = []) {
    return {
        title: "Matrix",
        poster: "http://image.tmdb.org/t/p/original/bfuMEU1Sc4K24j2lSIvDQDCD6ix.jpg",
        actors
    }
}

function secondMovieWithActors(actors = []) {
    return {
        title: "John Wick",
        poster: "http://image.tmdb.org/t/p/original/sv84oKUTmU9Yf5BPhfuJilGgxtV.jpg",
        actors
    }
}
