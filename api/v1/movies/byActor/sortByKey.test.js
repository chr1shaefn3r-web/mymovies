const sortByKey = require("./sortByKey")

describe("sortByKey", () => {

    it('should be defined', () => {
        expect(sortByKey).toBeDefined();
    });

    it('should return an empty array if given nothing', () => {
        expect(sortByKey()).toEqual(expect.any(Array));
    });

    it('should return an empty array if given empty array', () => {
        expect(sortByKey([])).toEqual(expect.any(Array));
    });

    it('should not change an already sorted object', () => {
        const mockMoviesByActor = {
            "Keanu Reeves": { movies: [] }
        };
        expect(sortByKey(mockMoviesByActor)).toEqual([{ actor: "Keanu Reeves", movies: [] }]);
    });

    it('should not change an already sorted object - with movie', () => {
        const mockMoviesByActorWithMovie = {
            "Keanu Reeves": {
                movies: [{
                    title: "Matrix",
                    year: "2003"
                }]
            }
        };
        expect(sortByKey(mockMoviesByActorWithMovie)).toEqual([{
            actor: "Keanu Reeves", movies: [{
                title: "Matrix",
                year: "2003"
            }]
        }]);
    });

    it('should sort by actor', () => {
        const mockMoviesByActorWithMovie = {
            "Keanu Reeves": { movies: [] },
            "Laurence Fishburne": { movies: [] }
        };
        expect(sortByKey(mockMoviesByActorWithMovie)).toEqual([{
            actor: "Keanu Reeves", movies: []
        }, {
            actor: "Laurence Fishburne", movies: []
        }]);
    });

    it('should remove actors from movie', () => {
        const mockMoviesByActorWithMovie = {
            "Keanu Reeves": { movies: [{id: "123", actors: []}] },
            "Laurence Fishburne": { movies: [] }
        };
        expect(sortByKey(mockMoviesByActorWithMovie)).toEqual([{
            actor: "Keanu Reeves", movies: [{id: "123"}]
        }, {
            actor: "Laurence Fishburne", movies: []
        }]);
    });
});
