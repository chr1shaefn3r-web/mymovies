const movies = require("../data.json");
const compose = require("../../../lib/compose");
const mapTransducer = require("../../../lib/mapTransducer");
const allowProperties = require("../../../lib/allowProperties");
const reduceAmountOfItemsInProperty = require("../../../lib/reduceAmountOfItemsInProperty");
const sortByKey = require("./sortByKey");
const mapToByActor = require("./mapToByActor");
const { significantActors } = require("../../config")

const atLeastTwoMovies = ({ movies }) => movies.length >= 2;

module.exports = () => {
    const removeUnneededProperties = mapTransducer(allowProperties(["id", "file", "title", "poster", "actors"]));
    const extras = 7;
    const onlySignifacantActorsAndExtras = mapTransducer(reduceAmountOfItemsInProperty("actors", significantActors+extras));
    const xform = compose(
        removeUnneededProperties,
        onlySignifacantActorsAndExtras
    )(mapToByActor);
    const sortedArray = sortByKey(movies.reduce(xform, {}));
    return sortedArray
        .filter(atLeastTwoMovies);
}
