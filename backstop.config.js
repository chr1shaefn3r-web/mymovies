const packageJson = require("./package.json");

const progressBarSelector = ".progressbar-container";

module.exports = {
    id: packageJson.name,
    viewports: [
        {
            "label": "desktop",
            "width": 1920,
            "height": 1080
        }
    ].concat(...device("SmallSmartphone", 320, 568))
        .concat(...device("NormalSmartphone", 480, 800))
        .concat(...device("NormalTablet", 768, 1024)),
    scenarios: [
        scenario("movies"),
        scenarioSubpage("movies/byYear"),
        scenarioSubpage("movies/byActor"),
        scenarioSubpage("movies/byDirector"),
        scenario("collections"),
        scenarioSubpage("collections/imdbTop250Eng", progressBarSelector),
        scenarioBySelector("collections/imdbTop250Eng", progressBarSelector),
        scenarioSubpageByUrl("Matrix-Movienightrequest", "/movienights/request?id=tt0133093&id=tt0234215&id=tt0242653")
    ],
    paths: {
        bitmaps_reference: "backstop/references",
        bitmaps_test: "backstop/tests",
        html_report: "backstop/results/html_report",
        ci_report: "backstop/results/ci_report"
    },
    report: [
        "CI"
    ],
    engine: "puppeteer",
    engineOptions: {
        args: [
            "--no-sandbox"
        ]
    },
    asyncCaptureLimit: 15,
    asyncCompareLimit: 50,
	delay: 5000,
    debug: false,
    debugWindow: false
};

function device(label, width, height) {
    return [{
        label, width, height
    }, {
        label: label + "Landscape", width: height, height: width
    }]
}

function scenario(label) {
    return scenarioWithRemoveSelectors(label);
}

function scenarioSubpageByUrl(label, url) {
    return scenarioWithRemoveSelectors(label, undefined, [".mainNav"], url);
}

function scenarioSubpage(label, additionalRemoveSelectors = []) {
    return scenarioWithRemoveSelectors(label, undefined, [".mainNav"].concat(additionalRemoveSelectors));
}

function scenarioBySelector(label, selector) {
    return scenarioWithRemoveSelectors(label, selector);
}

function scenarioWithRemoveSelectors(label, selector = "body", removeSelectors = [], url) {
    return {
        label,
        url: "https://mymovies.chr1shaefn3r.now.sh/" + (url || label),
        misMatchThreshold: 0.1,
        selectors: [selector],
        removeSelectors
    }
}
