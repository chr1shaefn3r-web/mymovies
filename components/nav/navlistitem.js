import Link from "next/link";

const Navlistitem = ({ title, currentPage, asPath }) => <li>
    <Link legacyBehavior href={asPath || "/" + title.toLowerCase()}><a className={title === currentPage ? "active" : ""}>{title}</a></Link>
    <style jsx>{`
        li > a {
            display: block;
            padding-top: .5rem;
            text-decoration: none;
            background-color: transparent;
            color: rgba(255,255,255,.7);
        }
        .active {
            color: #fff!important;
            font-weight: bold!important;
        }
        @media screen and (max-width: 370px) {
          li > a {
                padding-top: .25rem;
            }
        }
    `}</style>
</li>;

export default Navlistitem;
