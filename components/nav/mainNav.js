import Package from '../../package.json'

const MainNav = ({ firstLevel, children }) => <nav className="mainNav">
    {firstLevel}
    {children}
    <style jsx>{`
        .mainNav {
            font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
            font-size: 1rem;
            line-height: 1.5;
            padding: .5rem 1rem;
            background-color: ${Package.theme};
        }
        @media screen and (max-width: 370px) {
            .mainNav {
                padding: .25rem .5rem;
            }
        }
        @media screen and (max-width: 300px) {
            .mainNav {
                padding: .2rem .2rem;
                font-size: .9rem;
            }
        }
    `}</style>
</nav>;

export default MainNav;