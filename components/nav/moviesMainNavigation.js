import Navlist from "./navlist"
import NavListItem from "./navlistitem"

const MoviesMainNavigation = ({ title }) => <Navlist>
    <NavListItem title="Home" asPath="/" currentPage={title} />
    <NavListItem title="Movies" currentPage={title} />
    <NavListItem title="Collections" currentPage={title} />
    <NavListItem title="About" currentPage={title} />
</Navlist>;

export default MoviesMainNavigation;