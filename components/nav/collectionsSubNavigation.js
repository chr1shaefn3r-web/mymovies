import Navlist from "./navlist"
import NavlistItem from "./navlistitem"

const CollectionsSubNavigation = ({ title }) => {
    return <Navlist>
        <NavlistItem title="IMDB Top250" asPath="/collections/imdbTop250" currentPage={title} />
        <NavlistItem title="IMDB Top250 Eng" asPath="/collections/imdbTop250Eng" currentPage={title} />
        <NavlistItem title="Oscar Best Picture" asPath="/collections/oscarBestPicture" currentPage={title} />
    </Navlist>
};

export default CollectionsSubNavigation;