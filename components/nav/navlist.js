const Navlist = ({ children }) => <ul>
    {children}
    <style jsx>{`
        ul {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            padding-left: 0;
            margin-bottom: 0;
            margin-top: 0;
            list-style: none;
            max-width: 350px;
        }
    `}</style>
</ul>;

export default Navlist;