import Navlist from "../nav/navlist"
import NavlistItem from "../nav/navlistitem"

const MoviesSubNavigation = ({ title }) => {
    return <Navlist>
        <NavlistItem title="By Year" asPath="/movies/byYear" currentPage={title} />
        <NavlistItem title="By Actor" asPath="/movies/byActor" currentPage={title} />
        <NavlistItem title="By Director" asPath="/movies/byDirector" currentPage={title} />
        <NavlistItem title="Nights" asPath="/movienights" currentPage={title} />
    </Navlist>
};

export default MoviesSubNavigation;