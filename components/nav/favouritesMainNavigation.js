import Navlist from "./navlist"
import NavListItem from "./navlistitem"

const FavouritesMainNavigation = ({ title }) => <Navlist>
    <NavListItem title="Home" asPath="/" currentPage={title} />
    <NavListItem title="About" currentPage={title} />
</Navlist>;

export default FavouritesMainNavigation;