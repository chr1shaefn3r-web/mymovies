import encodings from "../movieNights/request/encodings"

const username = "christoph";
const server = "morpheus.christophhaefner.de";
const copyCommand = "scp";
const localTarget = "./";

const tripleEscape = (file) => file.replace(/[ ()'&#]/g, '\\\\\\$&');

const DownloadCommands = ({ movies, selectedEncodings }) => {
    const commands = encodings
        .filter(encoding => selectedEncodings.includes(encoding.name))
        .map(({ searchDirectory, filter, encodingSpecificFilename }) => {
            const serverPathes = movies
                .filter(filter)
                .map(({ file }) => `${username}@${server}:${searchDirectory}*/${tripleEscape(encodingSpecificFilename(file))}`);
            if (serverPathes.length > 0) {
                return `${copyCommand} ${serverPathes.join(" ")} ${localTarget}`;
            }
        })
        .filter(Boolean);

    return (
        <>
            <pre className="commands">
                {commands.map((command, index) => <span key={index} className="command">{command}</span>)}
            </pre>
            <style jsx>{`
                .command {
                    display: block;
                    margin-top: 2em;
                }
                .commands {
                    display: block;
                    padding: 3em;
                    font-family: monospace;
                    white-space: pre-wrap;
                    font-family: Menlo, Monaco, "Courier New", Courier, monospace;
                    letter-spacing: -0.022em;
                    background: rgba(0, 0, 0, 0.05);
                    color: rgba(0, 0, 0, 0.84);
                    overflow-wrap: break-word;
                }
            `}</style>
        </>
    )
}

export default DownloadCommands;
