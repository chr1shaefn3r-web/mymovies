import { useRouter } from 'next/router';
import SelectMovie from './selectMovie'
import SelectEncoding from '../movieNights/request/selectEncoding'
import Collection from './collection'
import DownloadCommands from './downloadCommands'
import Sizing from './sizing'

import lookIdUpInMovies from '../api/lib/lookIdUpInMovies'
import isInMovies from '../api/lib/isInMovies'

import encodings from "../movieNights/request/encodings"

function toArray(idFromUrlQuery, defaultValue = []) {
    if (typeof idFromUrlQuery === "string") {
        return [idFromUrlQuery];
    }
    return idFromUrlQuery || defaultValue;
}

function goToRoute(router, selectedMovieIds, selectedEncodings) {
    router.push({
        pathname: '/movienights/request',
        query: {
            id: selectedMovieIds,
            encoding: selectedEncodings
        },
    }, undefined, { shallow: true })
}

const RequestMoviesForMovieNight = ({ movies }) => {

    const router = useRouter();
    const selectedMovieIds = toArray(router.query.id);
    const selectedMovies = selectedMovieIds
        .filter(isInMovies(movies))
        .map(lookIdUpInMovies(movies));

    const encodingsWithSizes = encodings
        .map((encoding) => {
            const moviesAvailableForThisEncoding = selectedMovies
                .filter((movie) => encoding.fileSizeMap[movie.id]);
            const size = moviesAvailableForThisEncoding
                .reduce((acc, movie) => {
                    return acc + encoding.fileSizeMap[movie.id];
                }, 0);
            if (size === 0) { return; }
            const amountOfMoviesAvailableForThisEncoding = moviesAvailableForThisEncoding.length;
            const { color, name, defaultSelected } = encoding;
            return { size, amountOfMoviesAvailableForThisEncoding, color, name, defaultSelected };
        }).filter(Boolean);

    const defaultSelectedEncodings = encodingsWithSizes.filter(encoding => encoding.defaultSelected);
    const allAvailableEncodings = defaultSelectedEncodings.map(encoding => encoding.name);
    const selectedEncodings = toArray(router.query.encoding, allAvailableEncodings);

    const selectedEncodingsWithSizes = encodingsWithSizes
        .filter(encoding => selectedEncodings.includes(encoding.name))

    return (
        <div className="requestMoviesForMovieNight">

            <SelectMovie
                movies={movies}
                selectedMovies={selectedMovies}
                onSuggestionSelected={(suggestions) => goToRoute(router, suggestions.map((movie) => movie.id), selectedEncodings)} />

            <h2>Selected Movies</h2>
            {selectedMovies.length === 0 &&
                <p>No movies selected yet.</p>
            }
            {selectedMovies.length > 0 &&
                <Collection foundMovies={selectedMovies} />
            }


            <h2>Select Encodings</h2>
            {selectedMovies.length > 0 &&
                <SelectEncoding
                    selectedEncodings={selectedEncodings}
                    encodingsWithSizes={encodingsWithSizes}
                    onEncodingSelected={(encodings) => goToRoute(router, selectedMovieIds, encodings)}
                />
            }

            <h2>Sizing</h2>
            {selectedMovies.length > 0 &&
                <Sizing encodingsWithSizes={selectedEncodingsWithSizes} />
            }

            <h2>Download Commands</h2>
            {selectedMovies.length > 0 &&
                <DownloadCommands movies={selectedMovies}
                    selectedEncodings={selectedEncodings} />
            }

            <style jsx>{`
                /* aligned with bootstrap grid category large */
                @media screen and (min-width: 992px) {
                    .requestMoviesForMovieNight {
                        max-width: 80%;
                        margin: auto;
                    }
                }
            `}</style>
        </div>
    )
};

export default RequestMoviesForMovieNight;