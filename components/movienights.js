import Link from "next/link";

const Movienights = () => <div className="movienights">
    <div className="groups">
        <Link legacyBehavior href="/movienights/CRV"><a className="md-link md-item md-avatar purple">CRV</a></Link>
        <Link legacyBehavior href="/movienights/CHKU"><a className="md-link md-item md-avatar orange">CHKU</a></Link>
        <Link legacyBehavior href="/movienights/CD"><a className="md-link md-item md-avatar teal">CD</a></Link>
    </div>
    <Link legacyBehavior href="/movienights/request"><a className="md-link md-item md-button grey">Request</a></Link>

    <style jsx>{`
        .orange {
            color: #fff;
            background-color: #FF9800;
        }
        .purple {
            color: #fff;
            background-color: #673ab7;
        }
        .teal {
            color: #fff;
            background-color: #009688;
        }
        .grey {
            color: #fff;
            background-color: #616161;
        }
        .md-link {
            text-decoration: none;
        }
        .md-item {
            justify-content: center;
            user-select: none;
            font-family: Roboto,sans-serif;
            overflow: hidden;
            display: inline-flex;
            align-items: center;
        }
        .md-avatar {
            height: 60px;
            width: 60px;
            border-radius: 50%;
            font-size: 1.25rem;
            margin-right: 0.25rem;
        }
        .md-button {
            height: 36px;
            letter-spacing: .0892857143em;
            font-size: .875rem;
            font-weight: 500;
            text-transform: uppercase;
            padding: 0 16px;
            margin: 16px 0px;
            box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);
            border-radius: 4px;
        }
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .movienights {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default Movienights;
