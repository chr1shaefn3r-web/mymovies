import MoviePoster from "./moviePoster"
import Title from "./title"
import Badge from "./badge"
import Runtime from "./runtime"
import MediaType from "./mediaType"

const Movie = (
    { id, title, poster, tagline, year, plot, genre, actors, runtime, mediaType, director }
) => {
    const genreUiText = (genre || []).join(", ");
    const actorsUiText = (actors || [])
        .map(({ name, role }) => `${name} (${role})`)
        .join(", ");
    return (
        <div className="movie">
            <MoviePoster id={id} title={title} url={poster} />

            <div className="content">
                <Title title={title} />
                <div className="tagline">{tagline}</div>
                <div className="description">{plot}</div>
                {actorsUiText && <div className="actors">{actorsUiText}</div>}
                <div className="extra">
                    <Badge key={year} text={year} />
                    <Badge key={genreUiText} text={genreUiText} />
                    <Badge key={director} text={director} />
                    <Runtime runtime={runtime} />
                    {
                        mediaType && <MediaType mediaType={mediaType} />
                    }
                </div>
            </div>

            <style jsx>{`
                .movie {
                    display: flex;
                    padding-bottom: .5rem;
                    padding-top: .5rem;
                    border-bottom: 1px solid #dee2e6;
                    content-visibility: auto;
                    /* absolute minimum height: 225px image height + 4px link height + 8px padding top+bottom + 1px grid */
                    contain-intrinsic-size: 246px;
                }
                .content {
                    padding-left: 1rem;
                }
                .tagline {
                    color: #4b535a;
                    overflow-wrap: break-word;
                }
                .description, .extra, .actors {
                    margin-top: .5rem;
                    max-height: 225px;
                    text-overflow: ellipsis;
                    white-space: wrap;
                    overflow: hidden;
                }
                /* aligned with bootstrap grid category small */
                @media screen and (max-width: 576px) {
                    .movie {
                        display: block;
                        text-align: center;
                    }
                    .content {
                        padding: 0em;
                    }
                }
            `}</style>
        </div >
    )
};

export default Movie;
