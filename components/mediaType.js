import Badge from "./badge"

const mapMediaTypeToDisplayText = {
    "dvd": "DvD",
    "bluray": "Blu-Ray",
    "uhd": "4K Ultra HD"
}

const MediaType = ({ mediaType }) => <Badge text={displayText(mediaType)} />;

export default MediaType;

function displayText(mediaType) {
    return mapMediaTypeToDisplayText[mediaType];
}