import Package from "../package.json"

const About = () => <section>
    <p>
        {Package.name} {Package.description}
    </p>
    <p>
        Version: {Package.version}
    </p>
    <p>
        Movies data from: {Package.moviesDataExportTime}<br />
        Movie file sizes from: {Package.movieFilesizeMap}
    </p>
    <p>
        IMDB Top 250 from: {Package.imdbTop250ExportTime}<br />
        IMDB Top 250 English from: {Package.imdbTop250EngExportTime}<br />
        Oscars Best Picture from: {Package.oscarBestPictureExportTime}
    </p>

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            section {
                max-width: 80%;
                margin-left: auto;
                margin-right: auto;
            }
        }
    `}</style>
</section>;

export default About;
