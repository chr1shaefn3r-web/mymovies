import Image from 'next/image'

const MoviePosterV2 = ({ id, link, url = "/static/NoPosterAvailable.png", title }) => <a href={link || 'https://imdb.com/title/' + id} target='_blank' rel="noreferrer">
    <Image decoding="async" loading="lazy" width="150px" height="225px" src={url.replace("original", "w200").replace("http://", "https://")} alt={title + " Poster"} />
</a>;

export default MoviePosterV2;
