import CollectionEntry from "./collectionEntry"
import Title from "./title"
import Badge from "./badge"

const Collection = (
    { name, foundMovies = [], missingMovies = [], chronologicalOrderTitles, children, MoviePosterComponent }
) => <div className="collection">
    {name && <Title title={name} />}
    <div className="collectionContent">
        {children}
        {
            foundMovies.map(({ id, file, title, poster }) =>
                <CollectionEntry key={id + file} id={id} title={title} url={poster} MoviePosterComponent={MoviePosterComponent} />
            )
        }
        {
            missingMovies.map((movieId) =>
                <CollectionEntry key={movieId} id={movieId} title={movieId} MoviePosterComponent={MoviePosterComponent} />
            )
        }
        {
            chronologicalOrderTitles &&
            <div>Chronological Order: {chronologicalOrderTitles.map(title => <Badge key={title} text={title} />)}</div>
        }
    </div>

    <style jsx>{`
        .collection {
            padding-bottom: 1rem;
            border-bottom: 1px solid #dee2e6;
            content-visibility: auto;
            /* absolute minimum height: 225px image height + 4 px link height + 4px margin top+bottom +
                38px header + 8px bottom padding + 16px bottom padding + 1px grid */
            contain-intrinsic-size: 300px;
        }
        .collectionContent {
            display: flex;
            flex-wrap: wrap;
            flex-direction: row;
            justify-content: flex-start;
        }
        @media screen and (max-width: 648px) {
            /* for small-screens, which means as soon
               as 3 movie posters (162px*4=648) don't fit next to each other anymore */
            .collectionContent {
                justify-content: space-evenly;
            }
        }
    `}</style>
</div >;

export default Collection;
