import Collection from "./collection";
import sortMovies from "../lib/sortMovies"

const MoviesByEncodings = ({ moviesByEncodings }) => <div className="moviesByEncodings">
    {
        moviesByEncodings.map(({ encoding, movies }) => <Collection key={encoding} id={encoding} name={encoding} foundMovies={sortMovies(movies)} />)
    }

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .moviesByEncodings {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default MoviesByEncodings;
