import Collection from "./collection";
import CollectionEntry from "./collectionEntry"
import sortMovies from "../lib/sortMovies"

const MoviesbyActor = ({ moviesByActor }) => <div className="moviesByActor">
    {
        moviesByActor.map(
            ({ actor, thumb, movies }) => {
                return (<Collection key={actor} id={actor} name={actor} foundMovies={sortMovies(movies)} >
                    {thumb && <CollectionEntry title={actor} url={thumb}
                        link={"https://www.themoviedb.org/search/person?query=" + encodeURIComponent(actor)} />
                    }
                </Collection>)
            }

        )
    }

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .moviesByActor {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default MoviesbyActor;
