import { useState } from 'react';
import Autosuggest from "react-autosuggest"


function getSuggestionValue(suggestion) {
    return suggestion.title;
}

function renderSuggestion(suggestion) {
    return (
        <span>{suggestion.title} ({suggestion.year})</span>
    );
}

const SelectMovie = ({ movies, selectedMovies, onSuggestionSelected }) => {
    const [suggestions, setSuggestions] = useState([]);
    const [value, setValue] = useState('');
    const inputProps = {
        placeholder: "Type a movie name",
        value,
        onChange
    };
    function getSuggestions(searchTerm) {
        const lowerCaseSearchTerm = searchTerm.toLowerCase();
        return movies.filter((movie) => searchTermIsInTitle(movie, lowerCaseSearchTerm) && notAlreadySelected(movie))
    };

    function searchTermIsInTitle(movie, lowerCaseSearchTerm) {
        return movie.title.toLowerCase().indexOf(lowerCaseSearchTerm) > -1;
    }

    function notAlreadySelected(movie) {
        return !selectedMovies.some((selectedMovie) => selectedMovie.id === movie.id)
    }

    function onChange(_event, { newValue }) {
        setValue(newValue);
    }
    function callbackOnSuggestionSelected(_event, data) {
        setValue('');
        const allSelectedMovies = [data.suggestion, ...selectedMovies];
        onSuggestionSelected(allSelectedMovies);
    }
    function onSuggestionsFetchRequested({ value }) {
        setSuggestions(getSuggestions(value));
    }
    return (
        <>
            <Autosuggest
                suggestions={suggestions}
                onSuggestionSelected={callbackOnSuggestionSelected}
                onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                onSuggestionsClearRequested={() => setSuggestions([])}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputProps} />

            <style global jsx>{`
                .react-autosuggest__container {
                    position: relative;
                }
                
                .react-autosuggest__input {
                    width: 240px;
                    height: 30px;
                    padding: 10px 20px;
                    font-family: Helvetica, sans-serif;
                    font-weight: 300;
                    font-size: 16px;
                    border: 1px solid #aaa;
                    border-radius: 4px;
                }
                
                .react-autosuggest__input--focused {
                    outline: none;
                }
                
                .react-autosuggest__input--open {
                    border-bottom-left-radius: 0;
                    border-bottom-right-radius: 0;
                }
                
                .react-autosuggest__suggestions-container {
                    display: none;
                }
                
                .react-autosuggest__suggestions-container--open {
                    display: block;
                    position: absolute;
                    top: 51px;
                    width: 280px;
                    border: 1px solid #aaa;
                    background-color: #fff;
                    font-family: Helvetica, sans-serif;
                    font-weight: 300;
                    font-size: 16px;
                    border-bottom-left-radius: 4px;
                    border-bottom-right-radius: 4px;
                    z-index: 2;
                }
                
                .react-autosuggest__suggestions-list {
                    margin: 0;
                    padding: 0;
                    list-style-type: none;
                }
                
                .react-autosuggest__suggestion {
                    cursor: pointer;
                    padding: 10px 20px;
                }
                
                .react-autosuggest__suggestion--highlighted {
                    background-color: #ddd;
                }
            `}</style>
        </>
    )
}

export default SelectMovie;
