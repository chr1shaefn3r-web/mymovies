import Collection from "./collection"
import Progressbar from "./progressbar"

const CollectionWithProgress = ({ percentage, color, amountOfMovies, amountOfMissingMovies, movies }) => <div className="collectionWithProgress">
    <Progressbar color={color} progress={percentage} amountOfMovies={amountOfMovies} amountOfMissingMovies={amountOfMissingMovies}/>
    <br />
    <Collection foundMovies={movies} />

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .collectionWithProgress {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default CollectionWithProgress;
