import Collection from "./collection";
import sortMovies from "../lib/sortMovies"

const MoviesbyYear = ({ moviesByYear }) => <div className="moviesByYear">
    {
        moviesByYear.map(({ year, movies }) => <Collection key={year} id={year} name={year} foundMovies={sortMovies(movies)} />)
    }

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .moviesByYear {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default MoviesbyYear;
