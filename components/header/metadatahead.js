import Head from 'next/head'
import Package from '../../package.json'

const Metadatahead = ({ title }) => <Head>
    <title>{title}</title>
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <meta name="description" content={Package.description} />
    <meta name="theme-color" content={Package.theme} />
    <link href="/static/icon/mymovies_96px_1x.png" rel="icon" type="image/png" />
    <link href="/static/icon/apple-touch-icon.png" rel="apple-touch-icon" type="image/png" />

    <link href="/manifest.json" rel="manifest" />
</Head>;

export default Metadatahead;