import Metadatahead from './metadatahead'
import MainNav from '../nav/mainNav'
import MoviesMainNavigation from '../nav/moviesMainNavigation'
import PageTitle from './pagetitle'

const Header = ({ title, children, externalTitleLink }) => <header>
  <Metadatahead title={title} />
  <MainNav title={title} firstLevel={<MoviesMainNavigation title={title} />} >{children}</MainNav>

  <PageTitle title={title}>
    {externalTitleLink &&
      <a href={externalTitleLink} className="externalLink">
        <img width="16" height="16" alt="External Link Symbol" src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfjCwQVFRsDiyupAAAAwklEQVQoz4WRsQ7BUBiFv2Kq0NkoYWkkRotJ5y4Soz6AJ+hmMInEwuAlJGwMZfYEXkCEQSLRrmpob/W2N+m5y/3POf+5+e8PBdCU7BQnvt01ykywqCbilzFNjhhRWWGBzYZPqj/gwhBPlC/6uSd0PML4ENLOySdCHji8VQYhm0CPQ9YQhT/p/Km0QSHLhm0S7uKKMdNY08XmCjQEJRvOtLITl/CpKz7bwBcJO1bMCSTZZMRALKvGDAtdMtxYsi/ac4wfr+QzOr1DYXwAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTktMTEtMDRUMjE6MjE6MjcrMDA6MDC+chyyAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE5LTExLTA0VDIxOjIxOjI3KzAwOjAwzy+kDgAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII=' />
      </a>
    }
  </PageTitle>

  <style jsx>{`
    .externalLink {
      margin-left: .5em;
    }
  `}</style>
</header>;

export default Header;
