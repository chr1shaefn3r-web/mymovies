import Metadatahead from './metadatahead'
import PageTitle from './pagetitle'
import MainNav from '../nav/mainNav'
import FavouritesMainNavigation from '../nav/favouritesMainNavigation'

const FavouritesHeader = ({ title }) => <header>
    <Metadatahead title={title} />
    <MainNav title={title} firstLevel={<FavouritesMainNavigation title={title} />}/>
    <PageTitle title={title} />
</header>;

export default FavouritesHeader;