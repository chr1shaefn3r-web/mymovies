const Pagetitle = ({ title, children }) => <div className="pageTitle">
    <h1>{title}</h1>
    {children}
    <style jsx>{`
        h1 {
            display: inline-block;
        }
        @media screen and (max-width: 370px) {
            h1 {
                font-size: 1.5em;
            }
        }
        @media screen and (min-width: 992px) {
            .pageTitle {
            max-width: 80%;
            margin-left: auto;
            margin-right: auto;
            }
        }
    `}</style>
</div>;

export default Pagetitle;