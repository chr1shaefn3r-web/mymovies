import Collection from "./collection";
import sortMovies from "../lib/sortMovies"

const MoviesbyMediaType = ({ moviesByMediaType }) => <div className="moviesByMediaType">
    {
        moviesByMediaType.map(({ mediaType, movies }) => <Collection key={mediaType} id={mediaType} name={mediaType} foundMovies={sortMovies(movies)} />)
    }

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .moviesByMediaType {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default MoviesbyMediaType;
