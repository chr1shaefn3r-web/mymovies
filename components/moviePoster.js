// Movie Poster Aspect Ratio:
// 225 / 150 = 1,5
const moviePosterStyle = {contentVisibility: "auto", containIntrinsicSize: "min(var(--main-width), 150px) min(calc(var(--min-width) * 1.5), 225px)"};

const MoviePoster = ({ id, link, url = "/static/NoPosterAvailable.png", title }) => <a href={link || 'https://imdb.com/title/' + id} target='_blank' rel="noreferrer">
    <img style={moviePosterStyle} decoding="async" loading="lazy" width="150px" height="225px" src={url.replace("original", "w200").replace("http://", "https://")} alt={title + " Poster"} />
</a>;

export default MoviePoster;
