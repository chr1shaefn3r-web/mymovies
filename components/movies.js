import Movie from "./movie";

const Movies = ({ sortedMovies }) => <div className="movies">
    {
        sortedMovies.map((movie) => <Movie key={movie.id + movie.file} {...movie} />)
    }

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .movies {
                max-width: 80%;
                margin: auto;
                text-align: justify;
            }
        }
    `}</style>
</div >;

export default Movies;
