import Badge from "./badge"

const Runtime = ({ runtime }) => <Badge text={displayText(runtime)} />;

export default Runtime;

function displayText(runtime) {
    const hours = Math.floor(runtime / 60);
    const minutes = runtime - (hours * 60);
    return `${hours}h ${minutes}min`
}