import styles from "../movieNights/request/sizing/progressbar.module.css"
import ProgressbarItem from "../movieNights/request/sizing/progressbarItem"
import ProgressbarLegend from "../movieNights/request/sizing/progressbarLegend"
import ProgressbarMinValue from "../movieNights/request/sizing/progressbarMinValue"
import ProgressbarMaxValue from "../movieNights/request/sizing/progressbarMaxValue"
import humanReadableFilesize from "../lib/humanReadableFileSize";

const kB = 1000;
const MB = kB * 1000;
const GB = MB * 1000;

const usbStickSizes = [{
    min: 0,
    max: 2 * GB
}, {
    min: (2 * GB) - 1,
    max: 4 * GB
}, {
    min: (4 * GB) - 1,
    max: 8 * GB
}, {
    min: (8 * GB) - 1,
    max: 16 * GB
}, {
    min: (16 * GB) - 1,
    max: 32 * GB
}, {
    min: (32 * GB) - 1,
    max: 64 * GB
}, {
    min: (64 * GB) - 1,
    max: 128 * GB
}, {
    min: (128 * GB) - 1,
    max: 256 * GB
}, {
    min: (256 * GB) - 1,
    max: 512 * GB
}, {
    min: (512 * GB) - 1,
    max: 1024 * GB
}, {
    min: (1024 * GB) - 1,
    max: 2048 * GB
}]

const Sizing = ({ encodingsWithSizes }) => {

    const overallDownloadSize = encodingsWithSizes
        .map(encoding => encoding.size)
        .reduce((acc, size) => acc + size, 0) * 1.1; // add 10% just to be safe

    let minimumUsbStickSize = 0;
    for (let i = 0; i < usbStickSizes.length; i++) {
        var usbStickSize = usbStickSizes[i];
        if (overallDownloadSize > usbStickSize.min && overallDownloadSize <= usbStickSize.max) {
            minimumUsbStickSize = usbStickSize.max
        }
    }
    const humanReadableMinimumUsbStickSize = humanReadableFilesize(minimumUsbStickSize);

    const encodingsWithSizesAndPercentages = encodingsWithSizes.map((encoding) => {
        const percentage = (encoding.size / minimumUsbStickSize) * 100;
        return { percentage, ...encoding }
    });

    return (
        <>
            <div>
                <span>Minimum USB-Stick size: <b>{humanReadableMinimumUsbStickSize}</b></span>
            </div>
            <div>
                {encodingsWithSizesAndPercentages.map((encoding) =>
                    <ProgressbarLegend key={encoding.name} {...encoding} />
                )}
            </div>
            <div className="progressbar">
                <div className={styles.progressbarContainerBorderColor}>
                    {encodingsWithSizesAndPercentages.map((encoding) =>
                        <ProgressbarItem key={encoding.name} {...encoding} />
                    )}
                </div>
                <ProgressbarMinValue value="0" />
                <ProgressbarMaxValue value={humanReadableMinimumUsbStickSize} />
            </div>

            <style jsx>{`
                .progressbar {
                    margin-bottom: 4em;
                }
            `}</style>
        </>
    );
}

export default Sizing;
