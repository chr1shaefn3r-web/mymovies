import Collection from "./collection";

const Collections = ({ sortedCollections }) => <div className="collections">
    {
        sortedCollections.map((collection) => <Collection key={collection.name} {...collection} />)
    }

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .collections {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default Collections;
