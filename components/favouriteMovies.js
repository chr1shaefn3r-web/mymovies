import Collection from "./collection";
import sortMovies from "../lib/sortMovies"
import { CopyToClipboard } from 'react-copy-to-clipboard';

const FavouriteMovies = ({ user, movies, localMoviesDifferFromServer }) => <div className="favouriteMovies">
    <Collection key="favouriteMovies" name="Favourite Movies" foundMovies={sortMovies(movies)} />
    {localMoviesDifferFromServer &&
        <div className="localMoviesActions">
            <div className="localMoviesAction">
                <CopyToClipboard text={JSON.stringify(movies)}>
                    <button>Copy to clipboard</button>
                </CopyToClipboard>
            </div>
            <div className="localMoviesAction">
                <button onClick={() => {
                    window.localStorage.removeItem(user)
                    window.location.reload()
                }}>Delete local movies</button>
            </div>
        </div>
    }

    <style jsx>{`
        .localMoviesActions {
            margin-top: 1rem;
        }
        .localMoviesAction {
            display: inline-block;
            margin-right: .5rem;
        }
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .favouriteMovies {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default FavouriteMovies;
