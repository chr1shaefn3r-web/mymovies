const Badge = ({ text }) => <div className="badge">
    {text}
    <style jsx>{`
        .badge {
            display: inline-block;
            padding: .5em;
            margin-right: .5em;
            margin-bottom: .5em;
            font-size: 75%;
            border-radius: .5em;
            color: #313131;
            background-color: #e8e8e8;
        }
    `}</style>
</div>;

export default Badge;
