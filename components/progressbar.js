const Progressbar = ({ color, progress, amountOfMovies, amountOfMissingMovies }) => {
    return (
        <div className="progressbar-container">
            <div className="progressbar-content progressbar-progress">
                {amountOfMovies > 0 &&
                <span>{amountOfMovies} Movies</span>}
            </div>
            <div className="progressbar-content ">
                {amountOfMissingMovies > 0 &&
                <span>{amountOfMissingMovies} wanted</span>}
            </div>
        <style jsx>{`
            .progressbar-container {
                border: 1px grey solid;
            }
            .progressbar-progress {
                background-color: ${color};
                width: ${progress}%;
                text-align: right;
            }
            .progressbar-content  {
                display: inline-block;
                padding: 2px 5px;
            }
        `}</style>
        </div>
    );
};

export default Progressbar;
