const Title = ({ title }) => <div className="header" id={title}>
    <a aria-label="Anchor" href={`#${title}`} >{title}</a>
    <style jsx>{`
        a {
            color: black;
            text-decoration: none;
        }
        .header {
            font-size: 2rem;
            margin-bottom: .5rem;
            font-weight: 500;
            line-height: 1.2;
            overflow-wrap: break-word;
        }
    `}</style>
</div>;

export default Title;
