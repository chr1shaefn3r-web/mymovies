import Collection from "./collection";
import CollectionEntry from "./collectionEntry";
import sortMoviesByYear from "../lib/sortMoviesByYear"

const MoviesbyDirector = ({ moviesByDirector }) => <div className="moviesByDirector">
    {
        moviesByDirector.map(
            ({ director, picture, linkToDirectorMovieDbPage, movies }) => {
                return (<Collection key={director} id={director} name={director} foundMovies={sortMoviesByYear(movies)} >
                    {picture && <CollectionEntry title={director} url={picture} link={linkToDirectorMovieDbPage} />}
                </Collection>)
            }

        )
    }

    <style jsx>{`
        /* aligned with bootstrap grid category large */
        @media screen and (min-width: 992px) {
            .moviesByDirector {
                max-width: 80%;
                margin: auto;
            }
        }
    `}</style>
</div >;

export default MoviesbyDirector;
