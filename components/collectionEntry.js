import MoviePoster from "./moviePoster"

const CollectionEntry = ({ id, title, url, link, MoviePosterComponent }) => {

    const MoviePosterRender = MoviePosterComponent || MoviePoster;

    return (<div className="small-img">
        <MoviePosterRender id={id} title={title} url={url} link={link} />

        <style jsx>{`
        .small-img {
            position: relative;
            display: inline-block;
            vertical-align: middle;
            background-color: transparent;
            margin: .25rem;
        }
    `}</style>
    </div >)
};

export default CollectionEntry;
