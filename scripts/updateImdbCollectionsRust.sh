#!/bin/bash

set -e
set -x

EXECUTEABLE="./imdb-data-collection/target/release/imdb-data-collection"

if [ ! -e "$EXECUTEABLE" ]
then
    (cd ./imdb-data-collection && cargo build --release)
fi

$EXECUTEABLE "https://www.imdb.com/chart/top" > api/v1/collections/imdbTop250/imdbTop250.json;
$EXECUTEABLE "https://www.imdb.com/chart/top-english-movies" > api/v1/collections/imdbTop250Eng/imdbTop250Eng.json;
$EXECUTEABLE "https://www.imdb.com/search/title/?groups=best_picture_winner&sort=year,desc&count=100&view=simple" > api/v1/collections/oscarBestPicture/oscarBestPicture.json;
