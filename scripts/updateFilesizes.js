const glob = require('glob')
const fs = require('fs')
const movies = require('./data.json');

const encodings = [{
    encoding: undefined,
    searchDirectory: "/home/share/filme"
}, {
    encoding: "h265",
    searchDirectory: "/home/share/filme.h265"
}, {
    encoding: "h265.stereo",
    searchDirectory: "/home/share/filme.h265.stereo"
}, {
    encoding: "br.raw",
    searchDirectory: "/raw/filme.br.raw"
}, {
    encoding: "uhd.h265",
    searchDirectory: "/home/share/filme.uhd.h265"
}, {
    encoding: "uhd.raw",
    searchDirectory: "/raw/filme.uhd.raw"
}];

encodings.forEach(({encoding, searchDirectory}) => {
    const movieFilesizeMap = {};
    const suffix = encoding ? '.' + encoding : '';
    console.log("Encoding: " + encoding);
    console.log("Search directory: " + searchDirectory);
    for (let i = 0; i < movies.length; i++) {
        const movie = movies[i];
        if (encoding !== undefined && movie.mediaType === "dvd") {
            continue;
        }

        const fileGlob = searchDirectory + "/*/" + movie.file.replace(".mkv", `${suffix}.mkv`);
        const files = glob.sync(fileGlob);
        if (files.length === 0) {
            if (encoding === "h265") console.log("No '" + encoding + "' for movie: '" + movie.title + "' (" + movie.file + ")");
            continue;
        }
        if (files.length !== 1) {
            throw new Error('There should be only one file, but there were: ' + files)
        }
        const movieFileAndPath = files[0];
        const stats = fs.statSync(movieFileAndPath);
        movieFilesizeMap[movie.id] = stats.size;
    }
    const resultFile = `/tmp/movieFilesizeMap${suffix}.json`;
    console.log("Result file: " + resultFile);
    fs.open(resultFile, 'w', (err, fd) => {
        if (err) throw err;
        fs.writeSync(fd, JSON.stringify(movieFilesizeMap), 0, 'utf8');
        fs.close(fd, (err) => {
            if (err) throw err;
        });
    });
});
