#!/bin/bash

set -e
set -x

mkdir -p temp
cd temp
pwd
scp morph:/home/share/kodi/**/videodb.xml ./
kodixml2json videodb.xml
cp videodb.json ../api/v1/movies/data.json
scp videodb.json morph:/tmp/data.json
scp ../scripts/updateFilesizes.js morph:/tmp/
ssh morph rm -f /tmp/movieFilesizeMap*.json
ssh morph node /tmp/updateFilesizes.js
scp morph:/tmp/movieFilesizeMap*.json ../api/v1/movies/
