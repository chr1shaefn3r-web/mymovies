# My Movies

 > A visualization of my videodb.xml (kodi export) for the web

<div align="center"><img width="55" style="padding-right: 1em" src="https://raw.githubusercontent.com/gilbarbara/logos/master/logos/jest.svg"/><img width="55" style="padding-right: 1em" src="https://raw.githubusercontent.com/gilbarbara/logos/master/logos/nextjs.svg"/><img width="55" src="https://raw.githubusercontent.com/gilbarbara/logos/master/logos/react.svg"/></div>

Icons via [TechStack](https://techstack-logos.web.app/)

## Upgrade dependencies

`npx npm-check-updates --enginesNode --packageManager yarn --doctor -u`