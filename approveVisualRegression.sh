#!/bin/bash

set -x # Print a trace of simple commands

backstop --config="backstop.config.js" approve
optipng ./backstop/references/*.png
